package org.norwaypackages.repositories;

import org.norwaypackages.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;

/**
 * Created by radc on 2015-10-26.
 */

public interface UserRepository extends Repository<User, Long>{
    //@Query("SELECT u FROM User u where u.nick = :username")
    User findOneByNick(String username);
}
