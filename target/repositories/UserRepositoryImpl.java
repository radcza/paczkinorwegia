package org.norwaypackages.repositories;

import org.norwaypackages.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by radc on 2015-10-30.
 */
@Repository
public class UserRepositoryImpl implements UserRepository{
    @Override
    @Query("SELECT u FROM User u where u.nick = :username")
    public User findOneByNick(String username) {
        return new User();
    }
}
