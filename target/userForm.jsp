<%--
  Created by IntelliJ IDEA.
  User: Radek
  Date: 2015-07-14
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Radoslaw Czarnecki">
    <title>Konto nowego użytkownika</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker3.css">
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.4.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>

    <script src="../../resources/js/bootstrap-datepicker.js"></script>
    <script src="../../resources/js/locales/bootstrap-datepicker.pl.min.js"></script>
    <!-- Custom CSS -->
    <link href="../../resources/css/shop-item.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    <style type="text/css">
        .myrow-container{
            margin: 20px;
        }
    </style>

    <script type="text/javascript" src="http://www.technicalkeeda.com/js/bootstrap/bootstrap-modal.js"></script>

</head>
<body class=".container-fluid">
<div class="container myrow-container">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">
                Dane nowego użytkownika
            </h3>
        </div>
        <div class="panel-body">
            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrSendModal">Użytkownik nadający paczkę</button>
            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrCarrierModal">Przewoźnik</button>
            <div class="modal fade" id="usrSendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Dane użytkownika nadającego paczkę:</h4>
                        </div>

                        <form:form method="post" id="userRegisterForm" cssClass="form-horizontal userRegisterForm" modelAttribute="user" action="saveUser">
                        <fieldset>
                            <div class="modal-body">
                                <c:if test="$(param.success eq true)">
                                <div class="alert alert-success">Konto założone poprawnie</div>
                                </c:if>
                                    <div class="form-group">

                                            <form:label path="nick" cssClass="control-label col-xs-3">Nick</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="nick" size="15"/>
                                            <form:errors path="nick"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="password" cssClass="control-label col-xs-3">Hasło</form:label>
                                        <div class="col-xs-6">
                                            <form:password cssClass="form-control" path="password" size="30" showPassword="true"/>
                                            <form:errors path="password"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="e_mail" cssClass="control-label col-xs-3">E-mail</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="e_mail" size="30"/>
                                            <form:errors path="e_mail"/>
                                        </div>
                                    </div>
                                 <div class="form-group">
                                        <form:label path="user_send.name" cssClass="control-label col-xs-3">Imię</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.name" size="20"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.surname" cssClass="control-label col-xs-3">Nazwisko</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.surname" size="20"/>

                                        </div>
                                    </div>


                                            <div class="form-group">
                                                <form:label path="user_send.birth_date" cssClass="control-label col-xs-3">Data urodzenia</form:label>
                                                <div class='col-xs-6'>
                                                    <div class="input-group date">
                                                        <form:input path="user_send.birth_date" type="text" class="form-control"/>
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                    </div>

                                                </div>
                                                <script type="text/javascript">
                                                    $(function () {

                                                        $('.input-group.date').datepicker({
                                                            format: "yyyy-mm-dd",
                                                            todayBtn: true,
                                                            clearBtn: true,
                                                            language: "pl",
                                                            orientation: "auto right",
                                                            todayHighlight: true
                                                        });
                                                    });
                                                </script>
                                            </div>

                                    <div class="form-group">
                                        <form:label path="user_send.country_birth" cssClass="control-label col-xs-3">Kraj pochodzenia</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.country_birth" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.city_birth" cssClass="control-label col-xs-3">Miasto urodzenia</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.city_birth" id="city_birthSelect" class="form-control">
                                                <option value=""></option>
                                            <c:forEach items="${CityList}" var="city">
                                                <option value="${city.name}">${city.name}</option>
                                            </c:forEach>
                                            </form:select>
                                            <%--<form:input cssClass="form-control" path="user_send.city_birth" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.country" cssClass="control-label col-xs-3">Kraj zamieszkania</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.country" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.city" cssClass="control-label col-xs-3">Miasto zamieszkania</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.city" id="citySelect" class="form-control">
                                                <option value=""></option>
                                                <c:forEach items="${CityList}" var="city">
                                                    <option value="${city.name}">${city.name}</option>
                                                </c:forEach>
                                            </form:select>
                                            <%--<form:input cssClass="form-control" path="user_send.city" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.postal_code" cssClass="control-label col-xs-3">Kod pocztowy</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.postal_code" size="6"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.street" cssClass="control-label col-xs-3">Ulica</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.street" size="20"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.nationality" cssClass="control-label col-xs-3">Narodowość</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.nationality" id="nationalitySelect" class="form-control">
                                                <option value="polska">polska</option>
                                                <option value="norweska">norweska</option>
                                            </form:select>
                                            <%--<form:input cssClass="form-control" path="user_send.nationality" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.gender" cssClass="control-label col-xs-3">Płeć</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.gender" id="disabledSelect" class="form-control">
                                                <option value="1">mężczyzna</option>
                                                <option value="2">kobieta</option>
                                            </form:select>
                                            <%--<form:input cssClass="form-control" path="user_send.gender" size="10"/>--%>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <form:hidden path="privilege" value="1" />

                                    </div>
                                    <div class="form-group">
                                        <form:label path="telephone" cssClass="control-label col-xs-3">Telefon</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="telephone" size="15"/>

                                        </div>
                                    </div>

                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-4">

                                        <%--<input type="submit" value="Save" class="btn btn-lg btn-primary"/>--%>
                                            <button type="submit" id="saveUser" class="btn btn-primary">Wyślij</button>
                                            <button type="reset" class="btn btn-default" >Wyczyść</button>
                                        </div>
                                        <div class="col-xs-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>


<script type="text/javascript" src="<c:url value="/resources/js/user.js"/>"></script>
<%--<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>--%>


<script type="text/javascript">
    $(document).ready(function(){
        $(".userRegisterForm").validate(
                {
                    rules: {
                        nick: {
                            required : true,
                            minlength: 5
                        },
                        password: {
                            required : true,
                            minlength: 8
                        },
                        e_mail: {
                            required : true,
                            email: true
                        }
                    },
                    messages: {
                        nick: {
                            required: "Proszę wprowadzić nick",
                            minlength: "Proszę wprowadzić minimum 5 znaków"
                        },
                        password: {
                            required: "Proszę wprowadzić hasło",
                            minlength: "Proszę wprowadzić minimum 8 znaków"
                        },
                        e_mail: {
                            required: "Proszę wprowadzić e-mail",
                            email: "Niepoprawny format e-mail"
                        }
                    },
                    highlight: function(element){
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element){
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                }

        );
    });

</script>
<%--<script type="text/javascript">
    $(function() {

        $('#datepicker').datepicker();

    });
</script>--%>
</body>
</html>
