<%--
  Created by IntelliJ IDEA.
  User: Radek
  Date: 2015-07-14
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Lista użytkowników</title>
  <!-- Bootstrap CSS -->
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <style type="text/css">
    .myrow-container {
      margin: 20px;
      navbutton_background_color: blue;
    }
  </style>
</head>
<body class=".container-fluid">
<div class="container myrow-container">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">
        <div align="left"><b>Lista użytkowników</b> </div>
        <div align="right"><a href="createUser">Dodaj użytkownika</a></div>
      </h3>
    </div>
    <div class="panel-body">
      <c:if test="${empty UserList}">
        There are no Users
      </c:if>
      <c:if test="${not empty UserList}">
        <table class="table table-hover table-bordered">
          <thead style="background-color: red;">
          <tr>
            <th>Id</th>
            <th>Nick</th>
            <th>Telefon</th>
            <th>Edytuj</th>
            <th>Usuń</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${UserList}" var="usr">
            <tr>
              <td><c:out value="${usr.id}" /></td>
              <td><c:out value="${usr.nick}" /></td>
              <td><c:out value="${usr.telephone}" /></td>

              <td><a href="editUser?id=${usr.id}">Edytuj</a></td>
              <td><a href="deleteUser?id=${usr.id}">Usuń</a></td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </c:if>
    </div>
  </div>
  <script src="<c:url value="/resources/js/jquery-2.1.4.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <script src="<c:url value="/resources/js/user.js"/>"></script>
</body>
</html>
