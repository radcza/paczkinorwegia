<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-15
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <%--<img src="/resources/img/troll.gif"  height="90" width="90" />--%>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="${pageContext.request.contextPath}/index">Paczki Norwegia</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="#">O firmie</a>
        </li>
      <security:authorize access="isAuthenticated()">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dodaj ogłoszenie <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="${pageContext.request.contextPath}/createAdvertisement">Dodaj paczkę</a></li>
            <li><a href="${pageContext.request.contextPath}/createAdvertisement2">Dodaj wyjazd</a></li>
            </ul>
        </li>
        </security:authorize>
        <li>
          <a href="#">bla-bla car</a>
        </li>
        <li>
          <a href="#">Rozkład busów</a>
        </li>
        <li>
          <a href="#">Kontakt</a>
        </li>
        <li >
          <a href="${pageContext.request.contextPath}/createUser">Załóż konto</a>
        </li>
        <security:authorize access="!isAuthenticated()">
        <li >
          <a href="${pageContext.request.contextPath}/login">Zaloguj</a>
        </li>
        </security:authorize>
        <security:authorize access="hasAnyRole('ROLE_USER','ROLE_ADMIN')">
          <li >
            <a href="${pageContext.request.contextPath}/profile">Moje konto</a>
          </li>
        </security:authorize>

        <security:authorize access="isAuthenticated()">
          <li >
            <a href="${pageContext.request.contextPath}/logout">Wyloguj (<security:authentication property="principal.username" />)</a>
          </li>
        </security:authorize>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>

<div class="container">
  <div class="row">
    <a href="#" class="pull-left"><img src="/resources/img/troll.gif" height="90" width="90"></a>
<security:authorize access="hasRole('ROLE_USER')">
    <div class="col-xs-8 col-xs-offset-2">
      <form action="getSelectedAdvertisements" class="form-horizontal" role="form">
      <div class="input-group">

        <select id="filter" name="filter" class="selectpicker show-tick">
          <optgroup label="Sortuj wg">
          <option value="1">Daty wystawienia</option>
          <option value="2">Czasu do końca</option>
          <option value="3">Wyjazd z (miasto)</option>
            <option value="4">Przyjazd do (miasto)</option>
            <option value="5">Komentarzy</option>
          </optgroup>
        </select>
        <script>
          $('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
          });
        </script>


        <input type="hidden" name="contain" value="" id="contain">
        <input type="text" class="form-control" id="contain2" name="contain2" placeholder="Czego szukasz?...">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </span>
      </div>
      </form>
    </div>
  </security:authorize>
  </div>
</div>

<script>
  $(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
      e.preventDefault();
      var param = $(this).attr("href").replace("#","");
      var concept = $(this).text();
      $('.search-panel span#search_concept').text(concept);
      $('.input-group #search_param').val(param);
    });
  });
</script>