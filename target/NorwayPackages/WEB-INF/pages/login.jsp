<%--suppress XmlPathReference --%>
<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-28
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
<style type="text/css">
  .error {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
  }

  .msg {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1;
  }

  .form-signin {
    max-width: 300px;
    padding: 19px 29px 29px;
    margin: 0 auto 20px;
    background-color: #fff;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    box-shadow: 0 1px 2px rgba(0,0,0,.05);
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin input[type="text"],
  .form-signin input[type="password"] {
    font-size: 16px;
    height: auto;
    margin-bottom: 15px;
    padding: 7px 9px;
  }

</style>
  <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
</head>

<c:url var="loginUrl" value="/login"/>
<c:if test="$(param.success eq true)">
  <div class="alert alert-success">Konto założone poprawnie</div>
</c:if>
  <form class="form-signin" action="${loginUrl}" method="post" role="form">
    <h3 class="form-signin-heading">Zaloguj się do portalu</h3>
    <div class="input-group input-sm">
      <label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
    <input type="text" id="username" name="username" class="form-control" placeholder="Nazwa użytkownika"/>
      </div>
    <div class="input-group input-sm">
    <label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label>
    <input type="password" id="password" name="password" class="form-control" placeholder="Hasło"/>
</div>
    <input type="hidden" name="${_csrf.parameterName}"   value="${_csrf.token}" />
    <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button>

  </form>


<%--<div class="container">
  <h2 class="form-signin-heading">
    Zaloguj się do Paczki Norwegia
  </h2>
  <spring:url var="authUrl" value="/static/j_spring_security_check"/>
  <form method="post" class="signin" action="${authUrl}">
    <fieldset>
      <table cellspacing="0">
        <tr>
          <th>
            <label for="username_or_email">Nazwa użytkownika lun e-mail</label></th>
            <td><input id="username_or_email" name="j_username" type="text"></td>

        </tr>
        <tr>
          <th><label for="password">Hasło</label></th>
          <td><input id="password" name="j_password" type="password"/>
          <small><a href="/account/resend_password">Zapomniełeś hasła?</a></small>
          </td>
        </tr>
        <tr>
          <th></th>
          <td><input id="remember_me" name="_spring_security_remember_me" type="checkbox">
          <label for="remember_me" class="inline">Pamiętaj mnie</label></td>
        </tr>
        <tr>
          <th></th>
        <td><input name="commit" type="submit" value=Zaloguj"/></td>
        </tr>
      </table>
    </fieldset>
  </form>--%>
 <%-- <script type="text/javascript">
    document.getElementById('username_or_email').focus();
  </script>--%>
