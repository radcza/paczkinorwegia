<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-11-17
  Time: 17:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body class=".container-fluid">
<div class="container">
  <div class="panel panel-success">

    <div class="panel-body well">


      <%--CARRIER USER--%>

              <h4 class="modal-title">Dane paczki do przewiezienia:</h4>


            <form:form method="post" id="packagesForm" cssClass="form-horizontal packagesForm" modelAttribute="packages" action="savePackages">
              <fieldset>
                <div class="modal-body">
                  <c:if test="${param.success eq true}">
                  <div class="alert alert-success">Ogłoszenie zostało zamieszczone</div>
                  </c:if>

                      <div class="form-group">
                          <form:label path="type" cssClass="control-label col-xs-3">
                              <c:if test="${type==1}">Podaj typ przewożonej paczki:</c:if>
                              <c:if test="${type==2}">Podaj typ nadwozia:</c:if>
                          </form:label>
                          <div class='col-xs-6'>
                              <form:input cssClass="form-control" path="type" size="15"/>
                          </div>

                      </div>

                  <div class="form-group">
                    <form:label path="weight" cssClass="control-label col-xs-3">
                        <c:if test="${type==1}">Podaj wagę paczki</c:if>
                        <c:if test="${type==2}">Podaj maksymalną wagę</c:if>
                    </form:label>
                    <div class='col-xs-6'>
                      <form:input cssClass="form-control" path="weight" size="15"/>
                    </div>

                  </div>

                      <c:if test="${type==2}">
                      <div class="form-group">
                          <form:label path="departureDate" cssClass="control-label col-xs-3">Data wyjazdu:</form:label>
                          <div class='col-xs-6'>
                              <div class="input-group date" id="date">
                                  <form:input path="departureDate" type="text" class="form-control"/>
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                              </div>

                          </div>
                          <script type="text/javascript">
                              $(function () {

                                  $('#date').datepicker({
                                      format: "yyyy-mm-dd",
                                      todayBtn: true,
                                      clearBtn: true,
                                      language: "pl",
                                      orientation: "auto right",
                                      todayHighlight: true
                                  });
                              });
                          </script>
                      </div>
                      </c:if>
                          <c:if test="${type==2}">
                      <div class="form-group">
                          <form:label path="freeSpace" cssClass="control-label col-xs-3">

                             Podaj wolną przestrzeń załadunku [%]
                          </form:label>
                          <div class='col-xs-6'>
                              <form:input cssClass="form-control" path="freeSpace" size="15"/>
                          </div>

                      </div>
                          </c:if>
                  <c:if test="${type==1}">
                      <form:hidden path="freeSpace" value="100" />
                  </c:if>
                  <div class="form-group">
                    <form:label path="description" cssClass="control-label col-xs-3">Wprowadź dodatkowy opis:</form:label>
                    <div class="col-xs-6">
                      <form:input cssClass="form-control" path="description" size="15"/>

                    </div>
                  </div>

                      <div class="form-group">
                          <form:hidden path="id_advertisement" value="${advertisement.id}" />

                      </div>


                  <div class="modal-footer">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-xs-4">

                            <%--<input type="submit" value="Save" class="btn btn-lg btn-primary"/>--%>
                          <button type="submit" id="savePackages" class="btn btn-primary">Wyślij</button>
                          <button type="reset" class="btn btn-default" >Wyczyść</button>
                        </div>
                        <div class="col-xs-4">
                        </div>
                      </div>
                    </div>
                  </div>
              </fieldset>
            </form:form>





<script type="text/javascript">
  $(document).ready(function(){




    $(".packageForm").validate(
            {
              rules: {
              /*  nick: {
                  required : true,
                  minlength: 5
                },
                password: {
                  required : true,
                  minlength: 8
                },
                e_mail: {
                  required : true,
                  email: true
                },
                telephone: {
                  required : true
                }*/
              },
              messages: {
               /* nick: {
                  required: "Proszę wprowadzić nick",
                  minlength: "Proszę wprowadzić minimum 5 znaków"
                },
                password: {
                  required: "Proszę wprowadzić hasło",
                  minlength: "Proszę wprowadzić minimum 8 znaków"
                },
                e_mail: {
                  required: "Proszę wprowadzić e-mail",
                  email: "Niepoprawny format e-mail"
                },
                telephone: {
                  required: "Proszę wprowadzić telefon komórkowy"
                }*/
              },
              highlight: function(element){
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
              },
              unhighlight: function(element){
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
              }
            }

    );
  });

</script>

</body>