<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-17
  Time: 13:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<head>
  <style type="text/css">
    .user-row {
      margin-bottom: 14px;
    }

    .user-row:last-child {
      margin-bottom: 0;
    }

    .dropdown-user {
      margin: 13px 0;
      padding: 5px;
      height: 100%;
    }

    .dropdown-user:hover {
      cursor: pointer;
    }

    .table-user-information > tbody > tr {
      border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
      border-top: 0;
    }


    .table-user-information > tbody > tr > td {
      border-top: 0;
    }
    .toppad
    {margin-top:20px;
    }

  </style>
</head>
<div class="container">
    <security:authentication var="user" property="principal.username" />
  <div class="row">
        <c:if test="${userObject==user}">
            <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
              <A href="edit.html" >Edytuj profil</A>

              <A href="/logout" >Wyloguj</A>

            </div>
        </c:if>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >


      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title">
            <c:choose>
              <%--$userObject.privilege=<security:authentication property="principal.privilege" />--%>
              <c:when test="${userObject.privilege==1}">
                <%--$userObject.name=<security:authentication property="principal.name" />--%>
                <c:out value="${userObject.user_send.name}" />
                <%--$userObject.surname=<security:authentication property="principal.surname" />--%>
                <c:out value=" ${userObject.user_send.surname}" />
              </c:when>
              <c:when test="${userObject.privilege==0}">
                <c:out value="${userObject.user_carrier.name}" />

              </c:when>
           </c:choose>
          </h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <c:if test="${userObject.privilege==0}">
            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="${userObject.user_carrier.logo}" class="img-rounded img-responsive"> </div>
            </c:if>
            <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
              <dl>
                <dt>DEPARTMENT:</dt>
                <dd>Administrator</dd>
                <dt>HIRE DATE</dt>
                <dd>11/12/2013</dd>
                <dt>DATE OF BIRTH</dt>
                   <dd>11/12/2013</dd>
                <dt>GENDER</dt>
                <dd>Male</dd>
              </dl>
            </div>-->
            <div class=" col-md-9 col-lg-9 ">
              <table class="table table-user-information">
                <tbody>
                <tr>
                  <td>Nick:</td>
                  <td><c:out value="${userObject.nick}" /></td>
                </tr>
                <tr>
                  <td>E-mail:</td>
                  <td><c:out value="${userObject.e_mail}" /></td>
                </tr>
            <c:if test="${userObject.privilege==1}">
                <tr>
                  <td>Zamieszkały w</td>
                  <td><c:out value="${userObject.user_send.country}" /></td>
                </tr>


                <tr>
<td></td>
                  <td><c:out value="${userObject.user_send.postal_code}" /> <c:out value=" ${userObject.user_send.city}" /></td>
                </tr>
                <tr>
                  <td></td>
                  <td><c:out value="${userObject.user_send.street}" /></td>
                </tr>
                <tr>
                  <td>Telefon</td>
                  <td><c:out value="${userObject.telephone}" /></td>
                </tr>

              </c:if>
                <c:if test="${userObject.privilege==0}">
                    <tr>
                        <td>Siedziba firmy/przewoźnika:</td>
                        <td><c:out value="${city}" /></td>
                    </tr>
                    <tr>
                        <td>Ulica:</td>
                        <td><c:out value="${userObject.user_carrier.street_name}" /></td>
                    </tr>
                    <tr>
                        <td>Telefon komórkowy:</td>
                        <td><c:out value="${userObject.user_carrier.phone_mobile}" /></td>
                    </tr>
                    <tr>
                        <td>Telefon stacjonarny:</td>
                        <td><c:out value="${userObject.user_carrier.phone_stationary}" /></td>
                    </tr>
                    <tr>
                        <td>Strona internetowa:</td>
                        <td><a href=" <c:out value="${userObject.user_carrier.www}" />"><c:out value="${userObject.user_carrier.www}" /></a></td>
                    </tr>
                    <tr>
                        <td>Opis dodatkowy:</td>
                        <td><c:out value="${userObject.user_carrier.description}" /></td>
                    </tr>
                    <tr>
                        <td>Maksymalna pojemność paki:</td>
                        <td><c:if test="${userObject.user_carrier.capacity==0}">
                            do 100 kg</c:if>
                            <c:if test="${userObject.user_carrier.capacity==1}">
                                100-500 kg</c:if>
                            <c:if test="${userObject.user_carrier.capacity==2}">
                                500-1000 kg</c:if>
                            <c:if test="${userObject.user_carrier.capacity==1}">
                                1-2 t</c:if>
                        </td>
                    </tr>
                    <tr>
                        <td>Czy posiada chłodnię:</td>
                        <td><c:if test="${userObject.user_carrier.refrigerated==0}">
                            NIE</c:if>
                            <c:if test="${userObject.user_carrier.refrigerated==1}">
                                TAK</c:if>
                       </td>
                    </tr>
                    <c:if test="${userObject.user_carrier.car_img!=null}">
                        <tr><td></td><td>

                            <img alt="User Pic" src="${userObject.user_carrier.car_img}" class="img-rounded img-responsive">
                        </td>
                        </tr>
                    </c:if>
                </c:if>
                </tbody>
              </table>

              <%--<a href="#showComments" class="btn btn-primary" data-toggle="modal">Komentarze</a>--%>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Komentarze <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#showComments" data-toggle="modal">Otrzymane</a></li>
                        <li><a href="#postedComments" data-toggle="modal">Wystawione</a></li>
                    </ul>
                </div>

              <a href="getSelectedAdvertisements?contain=<c:out value="${userObject.nick}"/>&contain2=<c:out value="${userObject.nick}"/>&filter=0&myoffers=1" class="btn btn-primary">
                  <c:if test="${userObject.nick==user}">Moje oferty </c:if>
                  <c:if test="${userObject.nick!=user}">Oferty wystawione przez <c:out value="${userObject.nick}"/></c:if>
                  </a>
              <c:if test="${userObject.nick==user}">
                <a href="getSelectedAdvertisements?contain=<c:out value="${userObject.id}"/>&contain2=<c:out value="${userObject.id}"/>&filter=111&myoffers=2" class="btn btn-primary">Obserwowane</a>
              </c:if>
            </div>
          </div>

            <div id="showComments" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Otrzymane komentarze</h4>
                        </div>

                                <div class="modal-body">
                                    <c:if test="${empty comments}">
                                        Brak otrzymanych komentarzy
                                    </c:if>
                                    <c:if test="${not empty comments}">


                                        <table class="table table-striped">
                                            <tr>
                                                <th>Ocena</th>
                                                <th>Komentarz</th>
                                                <th>Data wystawienia</th>
                                            </tr>
                                        <c:forEach items="${comments}" var="com">
                                                <tr>
                                                <td>
                                                    <c:out value="${com.points}" />
                                                </td>
                                                    <td>
                                                        <c:out value="${com.description}" />
                                                    </td>
                                                    <td>
                                                        <c:out value="${com.dateTime}" />
                                                    </td>
                                                </tr>
                                        </c:forEach>
                                        </table>
                                    </c:if>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cofnij</button>
                                    </div>
                                </div>

                    </div>

                </div>
            </div>

            <div id="postedComments" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Wystawione komentarze</h4>
                        </div>

                        <div class="modal-body">
                            <c:if test="${empty postedComments}">
                                Brak wystawionych komentarzy
                            </c:if>
                            <c:if test="${not empty postedComments}">


                                <table class="table table-striped">
                                    <tr>
                                        <th>Ocena</th>
                                        <th>Komentarz</th>
                                        <th>Data wystawienia</th>
                                    </tr>
                                    <c:forEach items="${postedComments}" var="pcom">
                                        <tr>
                                            <td>
                                                <c:out value="${pcom.points}" />
                                            </td>
                                            <td>
                                                <c:out value="${pcom.description}" />
                                            </td>
                                            <td>
                                                <c:out value="${pcom.dateTime}" />
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </c:if>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cofnij</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <c:if test="${userObject.nick==user}">
        <div class="panel-footer">
          <%--<a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>--%>
                        <span class="pull-right">
                            <a href="edit.html" data-original-title="Edytuj swoje dane" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <%--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--%>
                        </span>
        </div>
        </c:if>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
      //get data-for attribute
      var dataFor = $(this).attr('data-for');
      var idFor = $(dataFor);

      //current button
      var currentButton = $(this);
      idFor.slideToggle(400, function() {
        //Completed slidetoggle
        if(idFor.is(':visible'))
        {
          currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
        }
        else
        {
          currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
        }
      })
    });


    $('[data-toggle="tooltip"]').tooltip();

    $('button').click(function(e) {
      e.preventDefault();
     // alert("This is a demo.\n :-)");
    });
  });
</script>