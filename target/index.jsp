<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-15
  Time: 22:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col-md-3">
      <p class="lead">Ogłoszenia</p>
      <div class="list-group">
        <a href="#" class="list-group-item active">Paczki do wysłania</a>
        <a href="#" class="list-group-item">Przewoźnicy prywatni</a>
        <a href="#" class="list-group-item">Firmy przewozowe</a>
      </div>
      <security:authorize access="hasRole('ROLE_ADMIN')">

      <p class="lead">Panel administracyjny</p>
      <div class="list-group">
        <a href="${pageContext.request.contextPath}/users" class="list-group-item active">Użytkownicy</a>
        <a href="${pageContext.request.contextPath}/cities" class="list-group-item">Miasta</a>

      </div>
      </security:authorize>
    </div>


    <div class="col-md-9">
      <img class="img-responsive" src="http://placehold.it/800x300" alt="">
      <div class="thumbnail">
        <c:if test="${empty AdvertisementList}">
          Brak ogłoszeń
        </c:if>
        <c:if test="${not empty AdvertisementList}">
          <c:forEach items="${AdvertisementList}" var="adv">
            <div class="caption-full">
              <h4 class="pull-right">Do końca pozostało <c:out value="${adv.timeString}" /></h4>
              <h4><a href="#">Użytkownik: <c:out value="${adv.user.nick}" /></a>
              </h4>

              <p>Ogłoszenie dodano <c:out value="${adv.start}" /></p>
              <p>Przewóz z <c:out value="${adv.city_from.name}" /></p>


              <p> do <c:out value="${adv.city_to.name}" /></p>
              <p>Odległość: <c:out value="${adv.distance}" /></p>

              <p><strong>Opis paczki:</strong></p>
              <table class="table table-hover table-bordered">

                <tr>
                  <th>Waga</th>
                  <th>Typ</th>
                  <th>Opis dodatkowy</th>
                </tr>

                <c:forEach items="${adv.pack}" var="pac">
                  <tr>
                    <td><c:out value="${pac.weight}" /></td>
                    <td><c:out value="${pac.type}" /></td>
                    <td><c:out value="${pac.description}" /></td>
                  </tr>
                </c:forEach>
              </table>

            </div>
          </c:forEach>
        </c:if>
        <div class="ratings">
          <p class="pull-right">3 reviews</p>
          <p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            4.0 stars
          </p>
        </div>
      </div>

      <div class="well">

        <div class="text-right">
          <a class="btn btn-success">Leave a Review</a>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">10 days ago</span>
            <p>This product was great in terms of quality. I would definitely buy another!</p>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">12 days ago</span>
            <p>I've alredy ordered another one!</p>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">15 days ago</span>
            <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
          </div>
        </div>

      </div>

    </div>

  </div>

</div>
<!-- /.container -->


