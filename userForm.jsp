<%--
  Created by IntelliJ IDEA.
  User: Radek
  Date: 2015-07-14
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Radoslaw Czarnecki">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Konto nowego użytkownika</title>
  <!-- Bootstrap CSS -->
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../resources/css/shop-item.css" rel="stylesheet">
  <style type="text/css">
    .myrow-container{
      margin: 20px;
    }
  </style>
</head>
<body class=".container-fluid">
<div class="container myrow-container">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">
        Dane nowego użytkownika
      </h3>
    </div>
    <div class="panel-body">
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrSendModal">Użytkownik nadający paczkę</button>
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrCarrierModal">Przewoźnik</button>
        <div class="modal fade" id="usrSendModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Dane użytkownika nadającego paczkę:</h4>
                    </div>

      <form:form id="userRegisterForm" cssClass="form-horizontal" modelAttribute="user" method="post" action="saveUser">

          <div class="modal-body">
        <div class="form-group">
          <div class="control-label col-xs-3"> <form:label path="nick" >Nick</form:label> </div>
          <div class="col-xs-6">
            <form:hidden path="id" value="${userObject.id}"/>
            <form:input cssClass="form-control" path="nick" value="${userObject.nick}"/>
          </div>
        </div>
        <div class="form-group">
          <form:label path="password" cssClass="control-label col-xs-3">Hasło</form:label>
          <div class="col-xs-6">
            <form:input cssClass="form-control" path="password" value="${userObject.password}"/>
          </div>
        </div>
              <div class="form-group">
                  <form:label path="e_mail" cssClass="control-label col-xs-3">E-mail</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="e_mail" value="${userObject.e_mail}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.name" cssClass="control-label col-xs-3">Imię</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.name" value="${userObject.user_send.name}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.surname" cssClass="control-label col-xs-3">Nazwisko</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.surname" value="${userObject.user_send.surname}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.birth_date" cssClass="control-label col-xs-3">Data urodzenia</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.birth_date" value="${userObject.user_send.birth_date}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.country_birth" cssClass="control-label col-xs-3">Kraj pochodzenia</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.country_birth" value="${userObject.user_send.country_birth}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.city_birth" cssClass="control-label col-xs-3">Miasto urodzenia</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.city_birth" value="${userObject.user_send.city_birth}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.country" cssClass="control-label col-xs-3">Kraj zamieszkania</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.country" value="${userObject.user_send.country}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.city" cssClass="control-label col-xs-3">Miasto zamieszkania</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.city" value="${userObject.user_send.city}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.postal_code" cssClass="control-label col-xs-3">Kod pocztowy</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.postal_code" value="${userObject.user_send.postal_code}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.street" cssClass="control-label col-xs-3">Ulica</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.street" value="${userObject.user_send.street}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.nationality" cssClass="control-label col-xs-3">Narodowość</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.nationality" value="${userObject.user_send.nationality}"/>
                  </div>
              </div>
              <div class="form-group">
                  <form:label path="user_send.gender" cssClass="control-label col-xs-3">Płeć</form:label>
                  <div class="col-xs-6">
                      <form:input cssClass="form-control" path="user_send.gender" value="${userObject.user_send.gender}"/>
                  </div>
              </div>


        <div class="form-group">
          <form:hidden path="privilege" value="1" />

        </div>
        <div class="form-group">
          <form:label path="telephone" cssClass="control-label col-xs-3">Telefon</form:label>
          <div class="col-xs-6">
            <form:input cssClass="form-control" path="telephone" value="${userObject.telephone}"/>
          </div>
        </div>


</div>
                    <div class="modal-footer">
        <div class="form-group">
          <div class="row">
            <div class="col-xs-4">


              <button type="submit" id="saveUser" class="btn btn-primary" onclick="return submitUserForm();">Wyślij</button>
                <button type="reset" class="btn btn-default" >Wyczyść</button>
            </div>
            <div class="col-xs-4">
            </div>
          </div>
                    </div>
        </div>

      </form:form>
                </div>
            </div>
        </div>
    </div>
    </div>
  </div>
</div>
<script src="<c:url value="/resources/js/jquery-2.1.4.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/user.js"/>"></script>

</body>
</html>
