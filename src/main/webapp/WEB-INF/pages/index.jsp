<%@ page import="org.norwaypackages.entity.Advertisement" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-15
  Time: 22:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.time.Instant" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.temporal.ChronoUnit" %>
<%@ page import="java.time.LocalDateTime" %>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Ogłoszenia</p>
            <div class="list-group">
                <a href="/getAllAdvertisements?type=3" class="list-group-item">Wszystkie ogłoszenia</a>
                <a href="/getAllAdvertisements?type=1" class="list-group-item">Paczki do wysłania</a>
                <a href="/getAllAdvertisements?type=2" class="list-group-item">Przewoźnicy</a>
             <%--   <br>
               <form action="getSelectedAdvertisements" class="form-horizontal" role="form">
                    <div class="input-group" id="adv-search">

                        <input type="text" class="form-control " name="contain2" placeholder="Wprowadź słowo..." />
                        <div class="input-group-btn">
                            <div class="btn-group" role="group">
                                <div class="dropdown dropdown-lg">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">


                                        <div class="form-group">
                                            <label for="contain">Zawiera słowo</label>
                                            <input class="form-control" type="text" id="contain" name="contain"/>
                                        </div>


                                        <div class="form-group">
                                            <label for="filter">Sortuj wg</label>
                                            <select id="filter" name="filter" class="form-control">
                                                <option value="0" selected="selected">Numeru ogłoszenia</option>
                                                <option value="1">Daty zgłoszenia</option>
                                                <option value="2">Czasu do końca</option>
                                                <option value="3">Rodzaju paczki</option>
                                                <option value="4">Komentarzy</option>
                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary center"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </div>
                        </div>

                    </div>
                </form>--%>


                <style>
                    body {
                        padding-top: 50px;
                    }

                    .mix {
                        min-height:370px;
                    }

                    .dropdown.dropdown-lg .dropdown-menu {
                        margin-top: -1px;
                        padding: 6px 20px;
                        margin: 5px;
                    }
                    .dropdown.dropdown-lg {
                        position: static !important;
                    }
                    .dropdown.dropdown-lg .dropdown-menu {
                        min-width: 350px;
                        border: 1px solid #1b5692;
                    }

                    .form-control{
                        border: 1px solid #1b5692;
                    }

                    .input-group-btn .btn-group {
                        display: flex !important;
                        background: #1b5692;
                    }
                    .btn-group .btn {
                        border-radius: 0;
                        margin-left: -1px;
                    }
                    .btn-group .btn:last-child {
                        border-top-right-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }
                    .btn-group .form-horizontal .btn[type="submit"] {
                        border-top-left-radius: 7px;
                        border-bottom-left-radius: 7px;
                    }
                    .form-horizontal .form-group {
                        margin-left: 0;
                        margin-right: 0;
                    }
                    .form-group .form-control:last-child {
                        border-top-left-radius: 7px;
                        border-bottom-left-radius: 7px;
                        border: 1px solid #1b5692;
                    }


                </style>





            </div>
            <security:authorize access="hasRole('ROLE_ADMIN')">

                <p class="lead">Panel administracyjny</p>
                <div class="list-group">
                    <a href="${pageContext.request.contextPath}/users" class="list-group-item active">Użytkownicy</a>
                    <a href="${pageContext.request.contextPath}/cities" class="list-group-item">Miasta</a>

                </div>
            </security:authorize>



        </div>


        <div class="col-md-9">
            <img class="img-responsive" src="http://placehold.it/800x300" alt="">


            <br>

            <div class="thumbnail">
                <c:if test="${empty AdvertisementList}">
                    Brak ogłoszeń
                </c:if>
                <c:if test="${not empty AdvertisementList}">
                    <%int count=1; %>

                    <jsp:useBean id="date" class="java.util.Date" />
                    <fmt:formatDate value="${date}" pattern="yyyy-MM-dd" var="currentDate" />


                    <c:forEach items="${AdvertisementList}" var="adv">
                        <c:set var="myoffers" value="${myoffers}"/>
                        <c:if test="${(!(adv.getEnd()<currentDate) && adv.acquired==0) || myoffers==1}">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading<%=count%>">
                                        <h4 class="panel-title">

                                            <table class="table table-bordered">
                                                    <c:if test="${adv.getType()==1}">
                                                    <tr bgcolor="#add8e6"></c:if>
                                                        <c:if test="${adv.getType()==2}"><tr bgcolor="#ff7f50">
                                                        </c:if>
                                                   <c:if test="${adv.getType()==1}">
                                                        <td> Nick:<br><c:out value="${adv.user.nick}" /> </td>
                                                        <td> Zgłoszono:<br><c:out value="${adv.start}" /></td><td> Ważne do:<br><c:out value="${adv.end}" /></td>
                                                        <td> Załadunek <br><c:if test="${adv.city_from.country_id==1}">POLSKA <img src="/resources/img/Poland-64.png"  style="width:30px; height:30px;"></c:if><c:if test="${adv.city_from.country_id==2}">NORWEGIA <img src="/resources/img/Norway-flag-64.png"  style="width:30px; height:30px;"></c:if>, <c:out value="${adv.city_from.name}" /></td>
                                                        <td> Rozładunek <br><c:if test="${adv.city_to.country_id==1}">POLSKA <img src="/resources/img/Poland-64.png"  style="width:30px; height:30px;"></c:if><c:if test="${adv.city_to.country_id==2}">NORWEGIA <img src="/resources/img/Norway-flag-64.png"  style="width:30px; height:30px;"></c:if>, <c:out value="${adv.city_to.name}" /></td>
                                                   </c:if>
                                                   <c:if test="${adv.getType()==2}">
                                                        <td> Nick: <br> <c:out value="${adv.user.nick}" /> </td>
                                                        <td> Wyjazd z <br><c:if test="${adv.city_from.country_id==1}">POLSKA <img src="/resources/img/Poland-64.png"  style="width:30px; height:30px;"></c:if><c:if test="${adv.city_from.country_id==2}">NORWEGIA <img src="/resources/img/Norway-flag-64.png"  style="width:30px; height:30px;"></c:if>, <c:out value="${adv.city_from.name}" /></td>
                                                        <td> Wyjazd do <br><c:if test="${adv.city_to.country_id==1}">POLSKA <img src="/resources/img/Poland-64.png"  style="width:30px; height:30px;"></c:if><c:if test="${adv.city_to.country_id==2}">NORWEGIA <img src="/resources/img/Norway-flag-64.png"  style="width:30px; height:30px;"></c:if>, <c:out value="${adv.city_to.name}" /></td>
                                                       <c:forEach items="${adv.pack}" var="pac">
                                                           <td> Data wyjazdu: <br><c:out value="${pac.departureDate}" /></td>
                                                           <td> Wolna przestrzeń: <br><c:out value="${pac.freeSpace}" /> %</td>
                                                           <td> Max waga: <br><c:out value="${pac.weight}" /> t</td>
                                                       </c:forEach>

                                                    </c:if>
                                                        <td style="vertical-align: middle">
                                                            <a class="accordion-toggle accordion-caret collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<%=count%>" aria-expanded="false" aria-controls="collapse<%=count%>">
                                                                <button type="button" class="btn btn-default btn-lg" title="Szczegółowe informacje">
                                                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                                                </button>
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: middle">

                                                            <a href="${pageContext.request.contextPath}/addToObserved?id_advertisement=${adv.id}&id_user=${userObject.id}"><img src="/resources/img/binoculars.png" style="width:30px; height:30px;" alt="obserwacja" title="Dodaj do obserwowanych" /></a>

                                                        </td>
                                                    </tr>

                                                </table>




                                        </h4>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.collapse').on('show', function() {
                                                    var $t = $(this);
                                                    var header = $("a[href='#" + $t.attr("id") + "']");
                                                    header.find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down");
                                                }).on('hide', function(){
                                                    var $t = $(this);
                                                    var header = $("a[href='#" + $t.attr("id") + "']");
                                                    header.find(".icon-chevron-down").removeClass("icon-chevron-down").addClass("icon-chevron-right");
                                                });
                                            });
                                        </script>
                                    </div>

                                        <%--<div class="caption-full">--%>
                                    <div id="collapse<%=count%>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%=count%>">
                                        <div class="panel-body">


                                        <c:if test="${!(adv.getEnd()<currentDate)}">
                                            <h4 class="pull-right">Do końca pozostało
                                                <c:set var="startdate" value="${adv.getStart().toLocalDate()}"/>
                                                <c:set var="enddate" value="${adv.getEnd().toLocalDate()}"/>
                                                <%=ChronoUnit.DAYS.between(LocalDateTime.now().toLocalDate(),(LocalDate)pageContext.getAttribute("enddate"))%>
                                                    <%--adv.getEnd().toLocalDate().getDayOfYear()-adv.getStart().toLocalDate().getDayOfYear()--%>
                                                    <%--<c:out value="${ChronoUnit.DAYS.between(adv.getStart().toLocalDate(),adv.getEnd().toLocalDate())}" />--%>
                                                dni
                                            </h4>
                                        </c:if>
                                            <table style="border-collapse: collapse">
                                                <tr>
                                                    <td>
                                                        <h4><a href="/profile?nick=${adv.user.nick}">Użytkownik: <c:out value="${adv.user.nick}" /></a>
                                                        </h4>
                                                    </td>
                                                    <td style="padding-left: 30px">
                                                        Średnia ocen:
                                                        <c:if test="${avgOpinions.get(adv.getUser_id())!=null}">
                                                        <c:out value="${avgOpinions.get(adv.getUser_id())}" />/5
                                                    </c:if>
                                                        <c:if test="${avgOpinions.get(adv.getUser_id())==null}">
                                                            brak
                                                        </c:if>
                                                    </td>
                                                    <td style="padding-left: 30px">
                                                        <security:authentication var="user" property="principal.username" />
                                                    <c:if test="${adv.getAcquired()==userObject.id}">
                                                        <a href="#addOpinion<%=count%>" class="btn btn-primary" data-toggle="modal">Dodaj komentarz</a>
                                                    </c:if>
                                                    </td>
                                                </tr>
                                            </table>
                                            <p>Ogłoszenie dodano <c:out value="${adv.start}" /></p>
                                            <p>Przewóz z <c:out value="${adv.city_from.name}" /> do <c:out value="${adv.city_to.name}" /></p>
                                            <p>Odległość: <c:out value="${adv.distance}" /></p>

                                            <c:if test="${adv.getType()==1}"><p><strong>Opis paczki:</strong></p></c:if>
                                            <c:if test="${adv.getType()==2}"><p><strong>Opis przewoźnika:</strong></p></c:if>
                                            <table class="table table-hover table-bordered">

                                            <c:if test="${adv.getType()==1}">
                                                <tr>
                                                    <th>Waga</th>
                                                    <th>Typ</th>
                                                    <th>Opis dodatkowy</th>
                                                </tr>
                                            </c:if>
                                                <c:if test="${adv.getType()==2}">
                                                    <tr> <c:if test="${userObject.privilege==0}">
                                                        <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="${userObject.user_carrier.car_img}" class="img-rounded img-responsive"> </div>
                                                    </c:if>
                                                    </tr>
                                                    <tr>
                                                        <th>Maksymalne waga</th>
                                                        <th>Typ nadwozia</th>
                                                        <th>Opis dodatkowy</th>
                                                    </tr>
                                                </c:if>
                                                <c:forEach items="${adv.pack}" var="pac">
                                                    <tr>
                                                        <td><c:out value="${pac.weight}" /></td>
                                                        <td><c:out value="${pac.type}" /></td>
                                                        <td><c:out value="${pac.description}" /></td>

                                                    </tr>
                                                    <c:set var="pacweight" value="${pac.weight}"/>
                                                </c:forEach>

                                            </table>
                                            <c:if test="${userObject.privilege==0}">
                                                <form:form method="post" action="bid?${_csrf.parameterName}=${_csrf.token}" modelAttribute="auction" class="form-horizontal">

                                                    <table class="table table-hover">

                                                        <tr>

                                                            <td><h4>Cena za przewiezienie: <c:out value="${adv.price_send}" />
                                                                <c:if test="${adv.city_from.country_id==1}"> zł</c:if>
                                                                <c:if test="${adv.city_from.country_id==2}"> koron</c:if>
                                                            </h4></td>
                                                            <td>
                                                                    <%--<form method="post" id="acquire" action="acquire?id_user=${userObject.id}&id_advertisement=${adv.id}" class="form-horizontal">--%>
                                                                    <%--<button class='btn btn-primary btn-xs' type="submit" name="remove_levels" value="delete"><span class="fa fa-times" data-toggle="modal" data-target="#acquireAcceptation"></span>Przejmij ogłoszenie</button>--%>
                                                                        <c:if test="${!(adv.getEnd()<currentDate)}">
                                                                        <c:if test="${adv.getType()==1 || (adv.getType()==2 && pacweight>0)}">
                                                                            <a href="#acquireAcceptation<%=count%>" class="btn btn-primary" data-toggle="modal" data-remote="true">Przejmij ogłoszenie</a>
                                                                        </c:if>
                                                                        </c:if>
                                                            </td>

                                                            <c:if test="${adv.getType()==1}">
                                                            <td><h4>Aktualna oferta:
                                                                <c:set var="minPrice" scope="session" value="${adv.price_send}"/>
                                                                <%int numUsers=0; %>
                                                                <c:forEach items="${adv.auctions}" var="auc">
                                                                    <c:if test="${auc.price<minPrice}">
                                                                        <c:set var="minPrice" scope="session" value="${auc.price}"/>
                                                                        <%--<c:set var="bestuser" scope="session" value="${auc.idUser}"/>--%>

                                                                    </c:if>
                                                                </c:forEach>
                                                                <c:out value="${minPrice}" />
                                                                zł</h4></td>
                                                                <%--${fn:length(adv.auctions)}--%>
                                                            <td><c:if test="${not empty nrBidders.get(adv.id)}">
                                                               <b><c:out value="${nrBidders.get(adv.id)}" /> </b>osób licytuje, wygrywa <b><c:out value="${winningUser.get(adv.id)}" /></b>
                                                                </c:if>
                                                                <c:if test="${empty nrBidders.get(adv.id)}">
                                                                Nikt nie licytuje
                                                                </c:if>
                                                            </td>
                                                            <td>


                                                                <c:if test="${!(adv.getEnd()<currentDate)}">
                                                                     <label id="price" for="price">Twoja oferta:</label>

                                                                    <input type="text" name="price" size="15"/>




                                                                     <input type="hidden" name="id_user" value="${userObject.id}" />

                                                                      <input type="hidden" name="id_adv" value="${adv.id}" />




                                                                <button type="submit" id="bid" class="btn btn-primary">Licytuj</button>
                                                                </c:if>


                                                            </td>
                                                            </c:if>
                                                        </tr>

                                                    </table>




                                                </form:form>


                                            </c:if>
                                        </div><!--.accordion-inner-->



                                    </div><!--.accordion-body-->
                                        <%--</div>--%>

                                </div>


                            </div>

                            <div id="acquireAcceptation<%=count%>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Potwierdzenie przejęcia ogłoszenia</h4>
                                        </div>
                                        <form method="post" id="acquire" action="acquire?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal">
                                            <fieldset>
                                                <div class="modal-body">
                                                    <p>Czy na pewno chcesz przewieźć przesyłkę użytkownika <c:out value="${adv.user.nick}" /> za <c:out value="${adv.price_send}" />
                                                        <c:if test="${adv.city_from.country_id==1}"> zł</c:if>
                                                        <c:if test="${adv.city_from.country_id==2}"> koron</c:if>?</p>
                                                    <div class="form-group">
                                                        <input type="hidden" name="id_user" value="${userObject.id}"/>
                                                        <input type="hidden" name="id_advertisement" value="${adv.id}"/>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cofnij</button>
                                                        <button type="submit" class="btn btn-primary">Chcę przejąć ogłoszenie</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <div id="addOpinion<%=count%>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Dodanie komentarza</h4>
                                        </div>
                                        <form method="post" id="addComment" action="addComment?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal">
                                            <fieldset>
                                                <div class="modal-body">
                                                    <p>Oceń użytkownika <c:out value="${adv.user.nick}" /></p>
                                                    <div class="form-group">
                                                        <fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" var="currentDateTime" />
                                                        <input type="hidden" name="addedby" value="${userObject.id}"/>
                                                        <input type="hidden" name="id_advertisement" value="${adv.id}"/>
                                                        <input type="hidden" name="id_user" value="${adv.user_id}"/>
                                                        <input type="hidden" name="dateTime" value="${currentDateTime}"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label id="points" for="points"  >Twoja ocena (1-5):</label>
                                                        <input type="number" name="points" min="1" max="5" style="width: 40px"/><br>
                                                        <label id="description" for="description">Komentarz:</label>
                                                        <textarea name="description" rows="4" style="width: 80%">Wpisz komentarz...</textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cofnij</button>
                                                        <button type="submit" class="btn btn-primary">Dodaj opinię</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>

                                </div>
                            </div>

                        </c:if>
                        <%count++;%>



                    </c:forEach>
                </c:if>

                <%--   <c:url var="firstUrl" value="/pages/1" />
                   <c:url var="lastUrl" value="/pages/${index.totalPages}" />
                   <c:url var="prevUrl" value="/pages/${currentIndex - 1}" />
                   <c:url var="nextUrl" value="/pages/${currentIndex + 1}" />

                   <div class="pagination">
                       <ul>
                           <c:choose>
                               <c:when test="${currentIndex == 1}">
                                   <li class="disabled"><a href="#">&lt;&lt;</a></li>
                                   <li class="disabled"><a href="#">&lt;</a></li>
                               </c:when>
                               <c:otherwise>
                                   <li><a href="${firstUrl}">&lt;&lt;</a></li>
                                   <li><a href="${prevUrl}">&lt;</a></li>
                               </c:otherwise>
                           </c:choose>
                           <c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
                               <c:url var="pageUrl" value="/pages/${i}" />
                               <c:choose>
                                   <c:when test="${i == currentIndex}">
                                       <li class="active"><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                                   </c:when>
                                   <c:otherwise>
                                       <li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                                   </c:otherwise>
                               </c:choose>
                           </c:forEach>
                           <c:choose>
                               <c:when test="${currentIndex == index.totalPages}">
                                   <li class="disabled"><a href="#">&gt;</a></li>
                                   <li class="disabled"><a href="#">&gt;&gt;</a></li>
                               </c:when>
                               <c:otherwise>
                                   <li><a href="${nextUrl}">&gt;</a></li>
                                   <li><a href="${lastUrl}">&gt;&gt;</a></li>
                               </c:otherwise>
                           </c:choose>
                       </ul>
                   </div>--%>

            </div>

            <div class="well">



            </div>

        </div>

    </div>

</div>
<script>
    $('.list-group-item').on('click',function(e){
        var previous = $(this).closest(".list-group").children(".active");
        previous.removeClass('active'); // previous list-item
        $(e.target).addClass('active'); // activated list-item
    });

</script>

<!-- /.container -->


