<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-09-15
  Time: 22:58
  To change this template use File | Settings | File Templates.
--%>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">



  <!-- Footer -->
  <footer>
    <div class="container">

      <div class="text-center center-block">

        <br />
        <a href="https://www.facebook.com/paczkinorwegiapl"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>

        <a href="https://plus.google.com/+paczkinorwegia-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
      </div>

    </div>

    <div class="row">
      <div class="col-lg-12">
        <p>Copyright &copy; Radoslaw Czarnecki 2015</p>
      </div>
    </div>
  </footer>

</div>
<!-- /.container -->
