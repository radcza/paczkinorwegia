<%@ page import="org.norwaypackages.entity.City" %>
<%--
  Created by IntelliJ IDEA.
  User: Radek
  Date: 2015-07-23
  Time: 11:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Paczki Norwegia</title>

  <!-- Bootstrap Core CSS -->
  <link href="../../resources/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../../resources/css/shop-item.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Paczki Norwegia</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="#">O firmie</a>
        </li>
        <li>
          <a href="#">Dodaj paczkę</a>
        </li>
        <li>
          <a href="#">bla-bla car</a>
        </li>
        <li>
          <a href="#">Rozkład busów</a>
        </li>
        <li>
          <a href="#">Kontakt</a>
        </li>
          <li >
              <a href="/createUser">Załóż konto</a>
          </li>
          <li >
              <a href="/login">Zaloguj</a>
          </li>
      </ul>
    </div>
      <!-- /.navbar-collapse -->
  </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Ogłoszenia</p>
            <div class="list-group">
                <a href="#" class="list-group-item active">Paczki do wysłania</a>
                <a href="#" class="list-group-item">Przewoźnicy prywatni</a>
                <a href="#" class="list-group-item">Firmy przewozowe</a>
            </div>
        </div>

        <div class="col-md-9">
            <img class="img-responsive" src="http://placehold.it/800x300" alt="">
            <div class="thumbnail">
                <c:if test="${empty AdvertisementList}">
                    Brak ogłoszeń
                </c:if>
                <c:if test="${not empty AdvertisementList}">
                    <c:forEach items="${AdvertisementList}" var="adv">
                        <div class="caption-full">
                            <h4 class="pull-right">Do końca pozostało <c:out value="${adv.timeString}" /></h4>
                            <h4><a href="#">Użytkownik: <c:out value="${adv.user.nick}" /></a>
                            </h4>


                            <p>Przewóz z <c:out value="${adv.city_from.name}" /></p>


                            <p> do <c:out value="${adv.city_to.name}" /></p>
                            <p>Odległość: <c:out value="${adv.distance}" /></p>

                            <p><strong>Opis paczki:</strong></p>
                            <table class="table table-hover table-bordered">

                                <tr>
                                    <th>Waga</th>
                                    <th>Typ</th>
                                    <th>Opis dodatkowy</th>
                                </tr>

                                <c:forEach items="${adv.pack}" var="pac">
                                    <tr>
                                        <td><c:out value="${pac.weight}" /></td>
                                        <td><c:out value="${pac.type}" /></td>
                                        <td><c:out value="${pac.description}" /></td>
                                    </tr>
                            </c:forEach>
                            </table>

                        </div>
                    </c:forEach>
        </c:if>
        <div class="ratings">
          <p class="pull-right">3 reviews</p>
          <p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            4.0 stars
          </p>
        </div>
      </div>

      <div class="well">

        <div class="text-right">
          <a class="btn btn-success">Leave a Review</a>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">10 days ago</span>
            <p>This product was great in terms of quality. I would definitely buy another!</p>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">12 days ago</span>
            <p>I've alredy ordered another one!</p>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            Anonymous
            <span class="pull-right">15 days ago</span>
            <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
          </div>
        </div>

      </div>

    </div>

  </div>

</div>
<!-- /.container -->

<div class="container">

  <hr>

  <!-- Footer -->
  <footer>
    <div class="row">
      <div class="col-lg-12">
        <p>Copyright &copy; Radoslaw Czarnecki 2015</p>
      </div>
    </div>
  </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="../../resources/js/jquery-2.1.4.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../resources/js/bootstrap.min.js"></script>

</body>

</html>
