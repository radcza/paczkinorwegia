<%@ page import="org.norwaypackages.entity.City" %>
<%@ page import="org.norwaypackages.entity.Advertisement" %>
<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-08-27
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.time.Instant" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.temporal.ChronoUnit" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Radoslaw Czarnecki">
    <title>Paczki Norwegia</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker3.css">
  <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
  <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.4.js"/>"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>

  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/locales/bootstrap-datepicker.pl.min.js"></script>
  <!-- Custom CSS -->
  <link href="../../resources/css/shop-item.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

  <![endif]-->
  <style type="text/css">
    .myrow-container{
      margin: 20px;
    }
  </style>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>


  <script type="text/javascript" src="<c:url value="/resources/js/user.js"/>"></script>
  <script type="text/javascript" src="http://www.technicalkeeda.com/js/bootstrap/bootstrap-modal.js"></script>
</head>


<body>
<tiles:insertAttribute name="top"/>

<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="footer"/>

</body>
</html>
