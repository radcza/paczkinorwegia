<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-07-18
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Lista miast</title>
  <!-- Bootstrap CSS -->
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <style type="text/css">
    .myrow-container {
      margin: 20px;
      navbutton_background_color: blue;
    }
  </style>
</head>
<body class=".container-fluid">
<div class="container myrow-container">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">
        <div align="left"><b>Lista miast</b> </div>
        <div align="right"><a href="createUser">Dodaj miasto</a></div>
      </h3>
    </div>
    <div class="panel-body">
      <c:if test="${empty CityList}">
        Brak wprowadzonych miast
      </c:if>
      <c:if test="${not empty CityList}">
        <table class="table table-hover table-bordered">
          <thead style="background-color: red;">
          <tr>
            <th>Id</th>
            <th>Miasto</th>
            <th>Region</th>
            <th>Współrzędne (Google Maps)</th>
            <th>Zapisz</th>
            <th>Usuń</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${CityList}" var="city">
            <tr>
              <form:form id="cityRegisterForm" cssClass="form-horizontal" modelAttribute="city" method="post" action="saveCity">

              <td><c:out value="${city.id}" /></td>
                <form:input cssClass="form-control" path="name" value="${cityObject.name}"/>
              <td><c:out value="${city.name}" /></td>
              <td><c:out value="${city.region}" /></td>
              <td><c:out value="${city.coordinates}" /></td>

              <td><a href="editCity?id=${city.id}">Edytuj</a></td>
              <td><a href="deleteCity?id=${city.id}">Usuń</a></td>
              </form:form>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </c:if>
    </div>
  </div>
  <script src="<c:url value="/resources/js/jquery-2.1.4.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <script src="<c:url value="/resources/js/user.js"/>"></script>
</body>
</html>
