<%--
  Created by IntelliJ IDEA.
  User: radc
  Date: 2015-11-17
  Time: 17:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body class=".container-fluid">
<div class="container">
  <div class="panel panel-success">

    <div class="panel-body well">


      <%--CARRIER USER--%>
      <c:if test="${type==1}">
      <h4 class="modal-title">Dane paczki do przewiezienia:</h4>
      </c:if>
        <c:if test="${type==2}">
        <h4 class="modal-title">Informacje o planowanym wyjeździe:</h4>
        </c:if>

      <form:form method="post" id="packageForm" cssClass="form-horizontal packageForm" modelAttribute="advertisement" action="saveAdvertisement">
      <fieldset>
        <div class="modal-body">
          <c:if test="${param.success eq true}">
          <div class="alert alert-success">Ogłoszenie zostało zamieszczone</div>
          </c:if>

          <div class="form-group">
            <form:label path="start" cssClass="control-label col-xs-3">Data rozpoczęcia wyświetlania ogłoszenia</form:label>
            <div class='col-xs-6'>
              <div class="input-group date" id="date1">
                <form:input path="start" type="text" class="form-control"/>
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              </div>

            </div>
            <script type="text/javascript">
              $(function () {

                $('#date1').datepicker({
                  format: "yyyy-mm-dd",
                  todayBtn: true,
                  clearBtn: true,
                  language: "pl",
                  orientation: "auto right",
                  todayHighlight: true
                });
              });
            </script>
          </div>
          <div class="form-group">
            <form:label path="end" cssClass="control-label col-xs-3">Data zakończenia wyświetlania ogłoszenia</form:label>
            <div class='col-xs-6'>
              <div class="input-group date" id="date2">
                <form:input path="end" type="text" class="form-control"/>
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              </div>

            </div>
            <script type="text/javascript">
              $(function () {

                $('#date2').datepicker({
                  format: "yyyy-mm-dd",
                  todayBtn: true,
                  clearBtn: true,
                  language: "pl",
                  orientation: "auto right",
                  todayHighlight: true
                });
              });
            </script>
          </div>


          <div class="form-group">
            <form:label path="city_id_from" cssClass="control-label col-xs-3">
              <c:if test="${type==1}">
              Skąd wysyłasz paczkę?
            </c:if>
              <c:if test="${type==2}">
                Skąd wyjeżdżasz?
              </c:if>
            </form:label>
            <div class="col-xs-6">
              <form:select path="city_id_from" id="id_from" class="form-control">
                <option value=""></option>
                <c:forEach items="${CityList}" var="city">
                  <option value="${city.id}">${city.name}</option>
                </c:forEach>
              </form:select>

            </div>
          </div>

          <div class="form-group">
            <form:label path="city_id_to" cssClass="control-label col-xs-3">
              <c:if test="${type==1}">
              Dokąd wysyłasz paczkę?
              </c:if>
              <c:if test="${type==2}">
                Dokąd wyjeżdżasz?
              </c:if>
            </form:label>
            <div class="col-xs-6">
              <form:select path="city_id_to" id="id_to" class="form-control">
                <option value=""></option>
                <c:forEach items="${CityList}" var="city">
                  <option value="${city.id}">${city.name}</option>
                </c:forEach>
              </form:select>

            </div>
          </div>

          <div class="form-group">
            <form:label path="distance" cssClass="control-label col-xs-3">Podaj przybliżoną odległość [km]</form:label>
            <div class='col-xs-6'>
              <form:input cssClass="form-control" path="distance" size="15"/>
            </div>

          </div>

            <div class="form-group">
              <form:label path="price_send" cssClass="control-label col-xs-3">Podaj cenę za przewiezienie przesyłki</form:label>
              <div class='col-xs-6'>
                <form:input cssClass="form-control" path="price_send" size="15"/>
              </div>

            </div>

          <div class="form-group">
            <c:if test="${type==1}">
            <form:hidden path="type" value="1" />
            </c:if>
            <c:if test="${type==2}">
              <form:hidden path="type" value="2" />
            </c:if>
            </div>
          </div>


          <div class="modal-footer">
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">

                    <%--<input type="submit" value="Save" class="btn btn-lg btn-primary"/>--%>
                  <button type="submit" id="saveAdvertisement" class="btn btn-primary">Dalej</button>
                  <button type="reset" class="btn btn-default" >Wyczyść</button>
                </div>
                <div class="col-xs-4">
                </div>
              </div>
            </div>
          </div>
      </fieldset>
      </form:form>





      <script type="text/javascript">
        $(document).ready(function(){




          $(".packageForm").validate(
                  {
                    rules: {
                      /*nick: {
                        required : true,
                        minlength: 5
                      },
                      password: {
                        required : true,
                        minlength: 8
                      },
                      e_mail: {
                        required : true,
                        email: true
                      },
                      telephone: {
                        required : true
                      }*/
                    },
                    messages: {
                     /* nick: {
                        required: "Proszę wprowadzić nick",
                        minlength: "Proszę wprowadzić minimum 5 znaków"
                      },
                      password: {
                        required: "Proszę wprowadzić hasło",
                        minlength: "Proszę wprowadzić minimum 8 znaków"
                      },
                      e_mail: {
                        required: "Proszę wprowadzić e-mail",
                        email: "Niepoprawny format e-mail"
                      },
                      telephone: {
                        required: "Proszę wprowadzić telefon komórkowy"
                      }*/
                    },
                    highlight: function(element){
                      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element){
                      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                  }

          );
        });

      </script>

</body>
