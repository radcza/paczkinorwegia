<%--
  Created by IntelliJ IDEA.
  User: Radek
  Date: 2015-07-14
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body class=".container-fluid">
<div class="container">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">
                Dane nowego użytkownika
            </h3>
        </div>
        <div class="panel-body well">
            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrSendModal">Użytkownik nadający paczkę</button>
            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#usrCarrierModal">Przewoźnik</button>
            <div class="modal fade" id="usrSendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Dane użytkownika nadającego paczkę:</h4>
                        </div>

                        <form:form method="post" id="userRegisterForm" cssClass="form-horizontal userRegisterForm" modelAttribute="user" action="saveUser">
                            <fieldset>
                                <div class="modal-body">
                                    <c:if test="$(param.success eq true)">
                                        <div class="alert alert-success">Konto założone poprawnie</div>
                                    </c:if>
                                    <div class="form-group">

                                        <form:label path="nick" cssClass="control-label col-xs-3">Nick</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="nick" size="15"/>
                                            <form:errors path="nick"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <form:label path="password" cssClass="control-label col-xs-3">Hasło</form:label>
                                        <div class="col-xs-6">
                                            <form:password cssClass="form-control" path="password" size="30" showPassword="true"/>
                                            <form:errors path="password"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="password_again" cssClass="control-label col-xs-3">Powtórz hasło</form:label>
                                        <div class="col-xs-6">
                                            <form:password cssClass="form-control" path="password_again" size="30" showPassword="true"/>
                                            <form:errors path="password_again"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="e_mail" cssClass="control-label col-xs-3">E-mail</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="e_mail" size="30"/>
                                            <form:errors path="e_mail"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.name" cssClass="control-label col-xs-3">Imię</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.name" size="20"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.surname" cssClass="control-label col-xs-3">Nazwisko</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.surname" size="20"/>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <form:label path="user_send.birth_date" cssClass="control-label col-xs-3">Data urodzenia</form:label>
                                        <div class='col-xs-6'>
                                            <div class="input-group date">
                                                <form:input path="user_send.birth_date" type="text" class="form-control"/>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>

                                        </div>
                                        <script type="text/javascript">
                                            $(function () {

                                                $('.input-group.date').datepicker({
                                                    format: "yyyy-mm-dd",
                                                    todayBtn: true,
                                                    clearBtn: true,
                                                    language: "pl",
                                                    orientation: "auto right",
                                                    todayHighlight: true
                                                });
                                            });
                                        </script>
                                    </div>

                                    <div class="form-group">
                                        <form:label path="user_send.country_birth" cssClass="control-label col-xs-3">Kraj pochodzenia</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.country_birth" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.city_birth" cssClass="control-label col-xs-3">Miasto urodzenia</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.city_birth" id="city_birthSelect" class="form-control">
                                                <option value=""></option>
                                                <c:forEach items="${CityList}" var="city">
                                                    <option value="${city.name}">${city.name}</option>
                                                </c:forEach>
                                            </form:select>
                                                <%--<form:input cssClass="form-control" path="user_send.city_birth" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.country" cssClass="control-label col-xs-3">Kraj zamieszkania</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.country" size="15"/>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <form:label path="user_send.city" cssClass="control-label col-xs-3">Miasto zamieszkania</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.city" id="citySelect" class="form-control">
                                                <option value=""></option>
                                                <c:forEach items="${CityList}" var="city">
                                                    <option value="${city.name}">${city.name}</option>
                                                </c:forEach>
                                            </form:select>
                                                <%--<form:input cssClass="form-control" path="user_send.city" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.postal_code" cssClass="control-label col-xs-3">Kod pocztowy</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.postal_code" size="6"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.street" cssClass="control-label col-xs-3">Ulica</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_send.street" size="20"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.nationality" cssClass="control-label col-xs-3">Narodowość</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.nationality" id="nationalitySelect" class="form-control">
                                                <option value="polska">polska</option>
                                                <option value="norweska">norweska</option>

                                            </form:select>
                                                <%--<form:input cssClass="form-control" path="user_send.nationality" size="15"/>--%>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_send.gender" cssClass="control-label col-xs-3">Płeć</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_send.gender" id="disabledSelect" class="form-control">
                                                <option value="1">mężczyzna</option>
                                                <option value="2">kobieta</option>
                                            </form:select>
                                                <%--<form:input cssClass="form-control" path="user_send.gender" size="10"/>--%>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <form:hidden path="privilege" value="1" />

                                    </div>

                                    <div class="form-group">
                                        <form:label path="telephone" cssClass="control-label col-xs-3">Telefon</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="telephone" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:hidden path="roles.role" value="ROLE_USER" />

                                    </div>
                                   <%-- <div class="form-group">
                                        <form:hidden path="roles.username" value="rad" />

                                    </div>--%>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-4">

                                                    <%--<input type="submit" value="Save" class="btn btn-lg btn-primary"/>--%>
                                                <button type="submit" id="saveUser" class="btn btn-primary">Wyślij</button>
                                                <button type="reset" class="btn btn-default" >Wyczyść</button>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>

            <%--CARRIER USER--%>
            <div class="modal fade" id="usrCarrierModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Dane przewoźnika:</h4>
                        </div>

                        <form:form id="userRegisterForm2" cssClass="form-horizontal userRegisterForm2" modelAttribute="user" action="/saveUser2?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">
                            <fieldset>
                                <div class="modal-body">
                                    <c:if test="$(param.success eq true)">
                                    <div class="alert alert-success">Konto założone poprawnie</div>
                                    </c:if>
                                    <div class="form-group">

                                        <form:label path="nick" cssClass="control-label col-xs-3">Nick</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="nick" size="15"/>

                                            <form:errors path="nick"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="password" cssClass="control-label col-xs-3">Hasło</form:label>
                                        <div class="col-xs-6">
                                            <form:password cssClass="form-control" path="password" size="30" showPassword="true"/>
                                            <form:errors path="password"/>

                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <form:label path="password_again" cssClass="control-label col-xs-3">Powtórz hasło</form:label>
                                            <div class="col-xs-6">
                                                <form:password cssClass="form-control" path="password_again" size="30" showPassword="true"/>
                                                <form:errors path="password_again"/>

                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <form:label path="e_mail" cssClass="control-label col-xs-3">E-mail</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="e_mail" size="30"/>
                                            <form:errors path="e_mail"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.name" cssClass="control-label col-xs-3">Nazwa firmy</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_carrier.name" size="20"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.city_id" cssClass="control-label col-xs-3">Miasto</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_carrier.city_id" id="city_carrier.city_id" class="form-control">
                                                <option value=""></option>
                                                <c:forEach items="${CityList}" var="city">
                                                    <option value="${city.id}">${city.name}</option>
                                                </c:forEach>
                                            </form:select>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <form:label path="user_carrier.street_name" cssClass="control-label col-xs-3">Ulica</form:label>
                                        <div class='col-xs-6'>
                                            <form:input cssClass="form-control" path="user_carrier.street_name" size="15"/>
                                        </div>

                                    </div>
                                          <%-- <div class="form-group">
                                               <form:label path="user_carrier.street_number" cssClass="control-label col-xs-3">Nr lokalu</form:label>
                                               <div class='col-xs-6'>
                                                   <form:input cssClass="form-control" path="user_carrier.street_number" size="10"/>
                                               </div>
                                           </div>--%>

                                    <div class="form-group">
                                        <form:label path="user_carrier.phone_mobile" cssClass="control-label col-xs-3">Telefon komórkowy</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_carrier.phone_mobile" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.phone_stationary" cssClass="control-label col-xs-3">Telefon stacjonarny</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_carrier.phone_stationary" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.www" cssClass="control-label col-xs-3">Strona internetowa:</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_carrier.www" size="15"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.NIP" cssClass="control-label col-xs-3">Numer NIP:</form:label>
                                        <div class="col-xs-6">
                                            <form:input cssClass="form-control" path="user_carrier.NIP" size="15"/>

                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <form:label path="user_carrier.NIP_doc" cssClass="control-label col-xs-3">Dokument NIP:</form:label>
                                            <div class="col-xs-6">

                                                <div style="position:relative;">
                                                    <a class='btn btn-primary' href='javascript:;'>
                                                        Wybierz plik...
                                                        <form:input path="user_carrier.NIP_doc" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'/>
                                                    </a>
                                                    &nbsp;
                                                    <span class='label label-info' id="upload-file-info"></span>
                                                </div>
                                                    <%--<form:input cssClass="form-control" path="user_carrier.logo" type="file" size="100"/>--%>

                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.insurance" cssClass="control-label col-xs-3">Ubezpieczenie:</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_carrier.insurance" id="insuranceSelect" class="form-control">
                                                <option value="0">tak</option>
                                                <option value="1">nie</option>
                                            </form:select>

                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <form:label path="user_carrier.Regon_doc" cssClass="control-label col-xs-3">Dokument Regon:</form:label>
                                            <div class="col-xs-6">

                                                <div style="position:relative;">
                                                    <a class='btn btn-primary' href='javascript:;'>
                                                        Wybierz plik...
                                                        <form:input path="user_carrier.Regon_doc" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'/>
                                                    </a>
                                                    &nbsp;
                                                    <span class='label label-info' id="upload-file-info"></span>
                                                </div>
                                                    <%--<form:input cssClass="form-control" path="user_carrier.logo" type="file" size="100"/>--%>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <form:label path="user_carrier.KRS_doc" cssClass="control-label col-xs-3">Dokument KRS:</form:label>
                                            <div class="col-xs-6">

                                                <div style="position:relative;">
                                                    <a class='btn btn-primary' href='javascript:;'>
                                                        Wybierz plik...
                                                        <form:input path="user_carrier.KRS_doc" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'/>
                                                    </a>
                                                    &nbsp;
                                                    <span class='label label-info' id="upload-file-info"></span>
                                                </div>
                                                    <%--<form:input cssClass="form-control" path="user_carrier.logo" type="file" size="100"/>--%>

                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.description" cssClass="control-label col-xs-3">Dodatkowy opis:</form:label>
                                        <div class="col-xs-6">
                                            <form:textarea cssClass="form-control" path="user_carrier.description" size="100"/>

                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <form:label path="user_carrier.logo" cssClass="control-label col-xs-3">Logo:</form:label>
                                            <div class="col-xs-6">

                                                <div style="position:relative;">
                                                    <a class='btn btn-primary' href='javascript:;'>
                                                        Wybierz plik...
                                                        <form:input path="user_carrier.logo" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'/>
                                                    </a>
                                                    &nbsp;
                                                    <span class='label label-info' id="upload-file-info"></span>
                                                </div>
                                                <%--<form:input cssClass="form-control" path="user_carrier.logo" type="file" size="100"/>--%>

                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.capacity" cssClass="control-label col-xs-3">Pojemność paki:</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_carrier.capacity" id="capacitySelect" class="form-control">
                                                <option value="0">do 100kg</option>
                                                <option value="1">100-500kg</option>
                                                <option value="2">500-1000kg</option>
                                                <option value="3">1-2t</option>
                                            </form:select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <form:label path="user_carrier.refrigerated" cssClass="control-label col-xs-3">Chłodnia:</form:label>
                                        <div class="col-xs-6">
                                            <form:select path="user_carrier.refrigerated" id="refrigeratedSelect" class="form-control">
                                                <option value="0">tak</option>
                                                <option value="1">nie</option>
                                            </form:select>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <form:hidden path="privilege" value="0" />

                                    </div>

                                        <div class="form-group">
                                            <form:hidden path="roles.role" value="ROLE_USER" />

                                        </div>

                                    <div class="modal-footer">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <%--<input type="hidden" name="${_csrf.parameterName}"   value="${_csrf.token}" />--%>
                                                        <%--<input type="submit" value="Save" class="btn btn-lg btn-primary"/>--%>
                                                    <button type="submit" id="saveUser2" class="btn btn-primary">Wyślij</button>
                                                    <button type="reset" class="btn btn-default" >Wyczyść</button>
                                                </div>
                                                <div class="col-xs-4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $(".userRegisterForm").validate(
                {
                    rules: {
                        nick: {
                            required : true,
                            minlength: 5
                        },
                        password: {
                            required : true,
                            minlength: 8
                        },
                        password_again: {
                            required : true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                        e_mail: {
                            required : true,
                            email: true
                        },
                        "user_send.name": {
                            required : true,
                            maxlength: 20
                        },
                        "user_send.surname": {
                            required : true,
                            maxlength: 20
                        },
                        "user_send.country": {
                            required : true,
                            maxlength: 15
                        },
                        "user_send.city": {
                            required : true,
                            maxlength: 15
                        },
                        "user_send.postal_code": {
                            required : true
                        },
                        "user_send.street": {
                            required : true,
                            maxlength: 20
                        },
                        "user_send.nationality": {
                            required : true
                        },
                        "user_send.telephone": {
                            required : true
                        }
                    },
                    messages: {
                        nick: {
                            required: "Proszę wprowadzić nick",
                            minlength: "Proszę wprowadzić minimum 5 znaków"
                        },
                        password: {
                            required: "Proszę wprowadzić hasło",
                            minlength: "Proszę wprowadzić minimum 8 znaków"
                        },
                        password_again: {
                            required: "Proszę wprowadzić powtórnie hasło",
                            minlength: "Proszę wprowadzić minimum 8 znaków",
                            equalTo: "Hasła nie są zgodne"
                        },
                        e_mail: {
                            required: "Proszę wprowadzić e-mail",
                            email: "Niepoprawny format e-mail"
                        },
                        "user_send.name": {
                            required : "Proszę wprowadzić imię",
                            maxlength: "Maksimum 20 znaków"
                        },
                        "user_send.surname": {
                            required : "Proszę wprowadzić nazwisko",
                            maxlength: "Maksimum 20 znaków"
                        },
                        "user_send.country": {
                            required : "Proszę wprowadzić kraj zamieszkania",
                            maxlength: "Maksimum 15 znaków"
                        },
                        "user_send.city": {
                            required : "Proszę wprowadzić miasto zamieszkania",
                            maxlength: "Maksimum 15 znaków"
                        },
                        "user_send.postal_code": {
                            required : "Proszę wprowadzić kod pocztowy"
                        },
                        "user_send.street": {
                            required : "Proszę wprowadzić ulicę",
                            maxlength: "Maksimum 20 znaków"
                        },
                        "user_send.nationality": {
                            required : "Proszę wprowadzić narodowość"
                        },
                        "user_send.telephone": {
                            required : "Proszę wprowadzić numer telefonu"
                        }
                    },
                    highlight: function(element){
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element){
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                }

        );



        $(".userRegisterForm2").validate(
                {
                    rules: {
                        nick: {
                            required : true,
                            minlength: 5
                        },
                        password: {
                            required : true,
                            minlength: 8
                        },
                        password_again: {
                            required : true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                        e_mail: {
                            required : true,
                            email: true
                        },
                        telephone: {
                            required : true
                        },
                        "user_carrier.name": {
                            required : true
                        },
                        "user_carrier.city_id": {
                            required : true
                        },
                        "user_carrier.street_name": {
                            required : true
                        },
                        "user_carrier.phone_mobile": {
                            required : true
                        },
                        "user_carrier.insurance": {
                            required : true
                        },
                        "user_carrier.capacity": {
                            required : true
                        },
                        "user_carrier.refrigerated": {
                            required : true
                        }
                    },
                    messages: {
                        nick: {
                            required: "Proszę wprowadzić nick",
                            minlength: "Proszę wprowadzić minimum 5 znaków"
                        },
                        password: {
                            required: "Proszę wprowadzić hasło",
                            minlength: "Proszę wprowadzić minimum 8 znaków"
                        },
                        password_again: {
                            required: "Proszę wprowadzić powtórnie hasło",
                            minlength: "Proszę wprowadzić minimum 8 znaków",
                            equalTo: "Hasła nie są zgodne"
                        },
                        e_mail: {
                            required: "Proszę wprowadzić e-mail",
                            email: "Niepoprawny format e-mail"
                        },
                        telephone: {
                            required: "Proszę wprowadzić telefon komórkowy"
                        },
                        "user_carrier.name": {
                            required : "Proszę wprowadzić nazwę firmy lub nazwisko"
                        },
                        "user_carrier.city_id": {
                            required : "Proszę wprowadzić miasto"
                        },
                        "user_carrier.street_name": {
                            required : "Proszę wprowadzić ulicę"
                        },
                        "user_carrier.phone_mobile": {
                            required : "Prosze wprowadzić numer telefonu"
                        },
                        "user_carrier.insurance": {
                            required : "Proszę wybrać czy firma jest ubezpieczona"
                        },
                        "user_carrier.capacity": {
                            required : "Prosze wprowadzic maksymalna pojemność paki"
                        },
                        "user_carrier.refrigerated": {
                            required : "Proszę określić czy samochód posiada chłodnię"
                        }
                    },
                    highlight: function(element){
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element){
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                }

        );
    });

</script>

</body>