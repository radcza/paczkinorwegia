/**
 * Created by Radek on 2015-07-14.
 */
function submitUserForm() {
    // getting the user form values
    var nick = $('#nick').val().trim();
    var telephone = $('#telephone').val();
    var address = $('#address').val();
    if(nick.length ==0) {
        alert('Proszę wprowadzić nick');
        $('#nick').focus();
        return false;
    }

    if(telephone.length ==0) {
        alert('Prosze wprowadzic telefon');
        $('#telephone').focus();
        return false;
    }

    /*if(address.length ==0) {
        alert('Proszę wprowadzić adres');
        $('#address').focus();
        return false;
    }*/
    return true;
};