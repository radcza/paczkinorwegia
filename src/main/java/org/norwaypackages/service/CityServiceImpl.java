package org.norwaypackages.service;

import org.norwaypackages.DAO.CityDAO;
import org.norwaypackages.DAO.CityDAOImpl;
import org.norwaypackages.DAO.UserDAO;
import org.norwaypackages.entity.City;
import org.norwaypackages.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by radc on 2015-07-18.
 */
@Service
@Transactional
public class CityServiceImpl implements CityService{
    @Autowired
    private CityDAO cityDAO;
    @Override
    public int createCity(City city) {
        return cityDAO.createCity(city);
    }

    @Override
    public City updateCity(City city) {
        return cityDAO.updateCity(city);
    }

    @Override
    public void deleteCity(int id) {
        cityDAO.deleteCity(id);
    }

    @Override
    public List getAllCities() {
        return cityDAO.getAllCities();
    }

    @Override
    public City getCity(int id) {
        return cityDAO.getCity(id);
    }
}
