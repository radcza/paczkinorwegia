package org.norwaypackages.service;

import org.norwaypackages.entity.City;

import java.util.List;

/**
 * Created by radc on 2015-07-18.
 */
public interface CityService {
    public int createCity(City city);
    public City updateCity(City city);
    public void deleteCity(int id);
    public List getAllCities();
    public City getCity(int id);
}
