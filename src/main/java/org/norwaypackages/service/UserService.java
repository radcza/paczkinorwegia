package org.norwaypackages.service;

import org.norwaypackages.entity.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by Radek on 2015-07-14.
 */
public interface UserService {
    public long createUser(User user);
    public User updateUser(User user);
    public void deleteUser(long id);
    public List getAllUsers();
    public User getUser(long id);
    public List getUserComments(long id);
    public List getPostedComments(long id);

}
