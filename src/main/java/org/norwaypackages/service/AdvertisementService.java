package org.norwaypackages.service;

import org.norwaypackages.entity.*;
import org.springframework.data.domain.Page;


import java.util.List;
import java.util.Map;

/**
 * Created by Radek on 2015-07-23.
 */
public interface AdvertisementService {
    public long createAdvertisement(Advertisement adv);

    public Advertisement updateAdvertisement(Advertisement adv);
    public void deleteAdvertisement(long id);
    public List getAllAdvertisements();
    public List getSelectedAdvertisements(int sortby, String search);
    public Advertisement getAdvertisement(long id);
    public User getUser(long user_id);
    public double findMinPrice(long id_advertisement);
    public Auction saveAuction(Auction auction);
   // public Page<Advertisement> getAdvertisements(Integer pageNumber);
   public Map<Long, Long> getNrBidding();
    public Map<Long, String> getUserWinning();
    public BestAuction updateBestAuction(long id, BestAuction ba);
    public Observed saveObserved(Observed observed);
    public Opinion saveOpinion(Opinion opinion);
    public Map<Long, Double> getAverageOpinion();
}
