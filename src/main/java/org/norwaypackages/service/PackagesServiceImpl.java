package org.norwaypackages.service;

import org.norwaypackages.DAO.AdvertisementDAO;
import org.norwaypackages.DAO.PackagesDAO;
import org.norwaypackages.entity.Packages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by radc on 2015-11-28.
 */
@Service
@Transactional
public class PackagesServiceImpl implements PackagesService{
    @Autowired
    private PackagesDAO packDAO;
    @Override
    public long createPackage(Packages pack) {
        return packDAO.createPackage(pack);
    }
}
