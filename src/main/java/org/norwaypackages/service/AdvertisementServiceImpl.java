package org.norwaypackages.service;

import org.norwaypackages.DAO.AdvertisementDAO;
import org.norwaypackages.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by Radek on 2015-07-23.
 */
@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService{
    @Autowired
    private AdvertisementDAO advDAO;
    private static final int PAGE_SIZE = 50;

    //@Autowired private AdvertisementRepositoryC advertisementRepository;
    @Override
    public long createAdvertisement(Advertisement adv) {
        return advDAO.createAdvertisement(adv);
    }




    @Override
    public Advertisement updateAdvertisement(Advertisement adv) {
        return advDAO.updateAdvertisement(adv);
    }

    @Override
    public void deleteAdvertisement(long id) {
        advDAO.deleteAdvertisement(id);
    }

    @Override
    public List getAllAdvertisements() {
        return advDAO.getAllAdvertisements();
    }

    @Override
    public List getSelectedAdvertisements(int sortby, String search) {
        return advDAO.getSelectedAdvertisements(sortby, search);
    }

    @Override
    public Advertisement getAdvertisement(long id) {
        return advDAO.getAdvertisement(id);
    }

    @Override
    public User getUser(long user_id) {
        return advDAO.getUser(user_id);
    }

    @Override
    public double findMinPrice(long id_advertisement){
        return advDAO.findMinPrice(id_advertisement);}

    @Override
    public Auction saveAuction(Auction auction) {
        return advDAO.saveAuction(auction);
    }

    /*   @Override
    public Page<Advertisement> getAdvertisements(Integer pageNumber) {
        PageRequest request =
                new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "end");
        return advertisementRepository.findAll(request);
    }*/
    public Map<Long, Long> getNrBidding() {
        return advDAO.getNrBidding();
    }
    public Map<Long, String> getUserWinning() {
        return advDAO.getUserWinning();
    }

    public BestAuction updateBestAuction(long id, BestAuction ba) {
        return advDAO.updateBestAuction(id, ba);
    }

    @Override
    public Observed saveObserved(Observed observed) {
        return advDAO.saveObserved(observed);
    }

    @Override
    public Opinion saveOpinion(Opinion opinion) {
        return advDAO.saveOpinion(opinion);
    }

    @Override
    public Map<Long, Double> getAverageOpinion() {
        return advDAO.getAverageOpinion();
    }
}
