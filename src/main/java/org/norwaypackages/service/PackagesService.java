package org.norwaypackages.service;

import org.norwaypackages.entity.Packages;

/**
 * Created by radc on 2015-11-28.
 */
public interface PackagesService {
    public long createPackage(Packages pack);
}
