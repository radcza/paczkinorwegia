package org.norwaypackages.repository;

import org.norwaypackages.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by radc on 2015-11-03.
 */
@Repository
public class JpaUserRepository implements UserRepository{
    @PersistenceContext
    private EntityManager em;
    public Iterable<User> findAll() {
        return em.createQuery(
                "from User", org.norwaypackages.entity.User.class
        ).getResultList();
    }

    public User findByNick(String username) {
        return em.createQuery(
                "from User where nick = :username", org.norwaypackages.entity.User.class
        ).setParameter("username", username).getSingleResult();
    }


}
