package org.norwaypackages.repository;

import org.norwaypackages.entity.User;
import org.springframework.data.repository.Repository;

/**
 * Created by radc on 2015-11-03.
 */

public interface UserRepository extends Repository<User, Long> {
    User findByNick(String username);
}
