package org.norwaypackages.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by radc on 2015-08-28.
 */
@Entity
@Table(name="user_send")
public class User_send implements Serializable{
    @Id
   // @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id", unique=true, nullable=false)
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign", parameters=@Parameter(name="property", value="user"))
    private long id;
    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private java.sql.Date birth_date;
    @Column
    private String country_birth;
    @Column
    private String city_birth;

    @Column
    private String country;
    @Column
    private String city;
    @Column
    private String postal_code;
    @Column
    private String street;
    @Column
    private String nationality;
    @Column
    private String gender;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public java.sql.Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(java.sql.Date birth_date) {

        this.birth_date = birth_date;
    }

    public String getCountry_birth() {
        return country_birth;
    }

    public void setCountry_birth(String country_birth) {
        this.country_birth = country_birth;
    }

    public String getCity_birth() {
        return city_birth;
    }

    public void setCity_birth(String city_birth) {
        this.city_birth = city_birth;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    @Override
    public String toString() {
        return "User_send{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birth_date=" + birth_date +
                ", country_birth='" + country_birth + '\'' +
                ", city_birth='" + city_birth + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", street='" + street + '\'' +
                ", nationality='" + nationality + '\'' +
                ", gender='" + gender + '\'' +
               // ", user=" + user +
                '}';
    }
}
