package org.norwaypackages.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by radc on 2015-07-09.
 */
@Entity
@Table(name = "user")
public class User implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column
    private String nick;
    @Column
   // @Size(min=6, max=20, message = "Hasło nie może być krótsze niż 6 znaków")
    private String password;
    private String password_again;
    @Column
    private String e_mail;
    @Column
    private String telephone;
    @Column
    private int privilege; //u-anonymous, a- admin, rc-registered client

    public User() {
    }


    public User_carrier getUser_carrier() {
            return user_carrier;
        }

        public void setUser_carrier(User_carrier user_carrier) {
            this.user_carrier = user_carrier;
        }

        public User_send getUser_send() {
            return user_send;
        }

        public void setUser_send(User_send user_send) {
            this.user_send = user_send;
        }

        @OneToOne(mappedBy = "user", cascade=CascadeType.ALL)
        //@JoinTable(name="user_carrier", joinColumns={@JoinColumn(name="id_user", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="id", referencedColumnName="id_user")})
        @Where(clause = "privilege='0'")
        private User_carrier user_carrier;

    @OneToOne(cascade=CascadeType.ALL, mappedBy = "user")
    //@PrimaryKeyJoinColumn
    //@JoinTable(name="user_send", joinColumns={@JoinColumn(name="id_user", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="id", referencedColumnName="id_user")})
    @Where(clause = "privilege='1'")
    private User_send user_send;

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    @OneToOne(cascade=CascadeType.ALL, mappedBy = "user")
    @JoinColumn(name="id_user", nullable = false)
    private Roles roles;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }






    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nick='" + nick + '\'' +
                ", password='" + password + '\'' +
                ", e_mail='" + e_mail + '\'' +
                ", telephone='" + telephone + '\'' +
                ", privilege=" + privilege +
                ", user_send=" + user_send +
                '}';
    }

    public String getPassword_again() {
        return password_again;
    }

    public void setPassword_again(String password_again) {
        this.password_again = password_again;
    }
}
