package org.norwaypackages.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by radc on 2016-01-07.
 */
@Entity
@Table(name = "auction")
public class Auction implements Serializable {
    private long id;
    private long idAdv;
    private long idUser;
    private double price;
    private Timestamp dateTime;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Column(name = "id_adv", nullable = false)
    public long getIdAdv() {
        return idAdv;
    }

    public void setIdAdv(long idAdv) {
        this.idAdv = idAdv;
    }


    @Column(name = "id_user", nullable = false)
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }


    @Column(name = "price", nullable = false)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Column(name = "date_time", nullable = false)
    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Auction auction = (Auction) o;

        if (id != auction.id) return false;
        if (idAdv != auction.idAdv) return false;
        if (idUser != auction.idUser) return false;
        if (price != auction.price) return false;
        if (dateTime != null ? !dateTime.equals(auction.dateTime) : auction.dateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (idAdv ^ (idAdv >>> 32));


        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        return result;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    //@PrimaryKeyJoinColumn
    @JoinColumn(name="id_user", referencedColumnName="id")
    private User user;
}
