package org.norwaypackages.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by radc on 2015-10-20.
 */
@Entity
@Table(name = "roles")
public class Roles implements Serializable {
    @Column
    private String username;
    @Column
    private String role;

    @Id
    @Column(name="id_user", unique=true, nullable=false)
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign", parameters=@Parameter(name="property", value="user"))
    private long id_user;

    public long getid_user() {
        return id_user;
    }

    public void setId_user(long idUser) {
        this.id_user = id_user;
    }

   // @Basic
  //  @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //@Basic
    //@Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

   /* @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Roles that = (Roles) o;

        if (idUser != that.idUser) return false;
        if (username != that.username) return false;
        if (role != that.role) return false;

        return true;
    }*/



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;
}
