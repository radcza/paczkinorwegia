package org.norwaypackages.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by radc on 2016-02-22.
 */
@Entity
@Table(name = "bestauction")
public class BestAuction implements Serializable {
    public long getId_adv() {
        return id_adv;
    }

    public void setId_adv(long id_adv) {
        this.id_adv = id_adv;
    }

    @Id
    @Column(name="id_adv", nullable=false)
    //@GeneratedValue(generator="gen")
    //@GenericGenerator(name="gen", strategy="foreign", parameters=@org.hibernate.annotations.Parameter(name="property", value="advertisement"))
    private long id_adv;

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Column
    long id_user;

    @Column
    double price;

    @Override
    public String toString() {
        return "BestAuction{" +
                "id_adv=" + id_adv +
                ", id_user=" + id_user +
                ", price=" + price +
                '}';
    }
}
