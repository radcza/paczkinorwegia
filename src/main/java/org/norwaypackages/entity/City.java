package org.norwaypackages.entity;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by radc on 2015-07-10.
 */
@Entity
@Table(name="city")
public class City implements Serializable {
    @Column
    public String name;
    @Column
    public String region;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column
    public String coordinates;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    @Column
    private int country_id;
}
