package org.norwaypackages.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by radc on 2016-02-25.
 */
@Table(name="opinion")
@Entity
public class Opinion implements Serializable{
    private long id;
    private long userId;
    private String description;
    private byte points;
    private long addedby;
    @Column
    private Timestamp dateTime;
    @Column
    private long id_adv;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }


    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Column(name = "points")
    public byte getPoints() {
        return points;
    }

    public void setPoints(byte points) {
        this.points = points;
    }


    @Column(name = "addedby")
    public long getAddedby() {
        return addedby;
    }

    public void setAddedby(long addedby) {
        this.addedby = addedby;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Opinion opinion = (Opinion) o;

        if (id != opinion.id) return false;
        if (userId != opinion.userId) return false;
        if (points != opinion.points) return false;
        if (addedby != opinion.addedby) return false;
        if (description != null ? !description.equals(opinion.description) : opinion.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) points;
        result = 31 * result + (int) (addedby ^ (addedby >>> 32));
        return result;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public long getId_adv() {
        return id_adv;
    }

    public void setId_adv(long id_adv) {
        this.id_adv = id_adv;
    }
}
