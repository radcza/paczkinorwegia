package org.norwaypackages.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by radc on 2016-02-23.
 */
@Entity
@Table(name = "observed")
public class Observed implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column
    long id;
    @Column
    long id_user;
    @Column
    long id_adv;

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }

    public long getId_adv() {
        return id_adv;
    }

    public void setId_adv(long id_adv) {
        this.id_adv = id_adv;
    }
}
