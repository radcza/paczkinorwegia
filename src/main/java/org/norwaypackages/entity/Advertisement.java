package org.norwaypackages.entity;

import org.apache.catalina.Store;
import org.jboss.logging.annotations.Field;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by radc on 2015-07-09.
 */
@Entity
//@Indexed
@Table(name = "advertisement")
public class Advertisement implements Serializable{



    @Column
    //@Field(index=Index.YES, analyze= Analyze.YES, store=Store.NO)
    private java.sql.Date start;

    public java.sql.Date getStart() {
        return start;
    }

    public void setStart(java.sql.Date start) {
        this.start = start;
    }

    public java.sql.Date getEnd() {
        return end;
    }

    public void setEnd(java.sql.Date end) {
        this.end = end;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

  /*  public int getId_from() {
        return id_from;
    }

    public void setId_from(int id_from) {
        this.id_from = id_from;
    }

    public int getId_to() {
        return id_to;
    }

    public void setId_to(int id_to) {
        this.id_to = id_to;
    }*/

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    @Column
    //@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    private java.sql.Date end;
    @Column
    private double price;
    @Column
    private boolean archive; //1-disappear when advertisement time ends or package is taken by carrier
    @OneToOne
    @JoinColumn(name="id_from")
    //@Column
    private City city_from;
    @OneToOne
    @JoinColumn(name="id_to")
   // @Column
    private City city_to;

    public int getCity_id_from() {
        return city_id_from;
    }

    public void setCity_id_from(int city_id_from) {
        this.city_id_from = city_id_from;
    }

    public int getCity_id_to() {
        return city_id_to;
    }

    public void setCity_id_to(int city_id_to) {
        this.city_id_to = city_id_to;
    }

    private int city_id_from;
    private int city_id_to;
    @Column
    public double distance;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @Column
   private long user_id;

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    @Column
    private short type;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToOne(cascade=CascadeType.ALL)
   // @JoinTable(name="user", joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="id", referencedColumnName="user_id")})
   // @JoinColumn(name="user_id")
    @JoinColumn(name="user_id", insertable=false, updatable=false)
    //@PrimaryKeyJoinColumn
    private User user;

    public long getTime() {
       /* long time = Math.abs(end.getTime () - System.currentTimeMillis());
        if(time>1000*60*60*24){
            if(time<2000*60*60*24) this.timeString="1 dzień";
            else this.timeString=(time/(1000*60*60*24))+" dni";
        }
        else this.timeString=(time/(1000*60*60))+" godzin";*/
        return time;
    }

    public void setTime(long time) {

        this.time = time;

    }

    private long time;

    public String getTimeString() {
        long time = Math.abs(end.getTime () - System.currentTimeMillis());
        if(time>1000*60*60*24){
            if(time<2000*60*60*24) return "1 dzień";
            else return (time/(1000*60*60*24))+" dni";
        }
        else return (time/(1000*60*60))+" godzin";

    }

    public void setTimeString(String timeString) {

        this.timeString = timeString;
    }

    public String timeString;
/*
    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }*/
/*
    @ElementCollection
    @CollectionTable(name = "cities", joinColumns = "id")
    @Column(name="cities")
    public List<City> cities=new ArrayList<>();*/

    @Override
    public String toString() {
        return "Advertisement{" +
                "start=" + start +
                ", end=" + end +
                ", price=" + price +
                ", archive=" + archive +
                //", id_from=" + id_from +
                //", id_to=" + id_to +
                ", distance=" + distance +
                ", id=" + id +
                ", user_id=" + user_id +
                '}';
    }

    public City getCity_from() {
        return city_from;
    }

    public void setCity_from(City city_from) {
        this.city_from = city_from;
    }

    public City getCity_to() {
        return city_to;
    }

    public void setCity_to(City city_to) {
        this.city_to = city_to;
    }


    public Set<Packages> getPack() {
        return pack;
    }

    public void setPack(Set<Packages> pack) {
        this.pack = pack;
    }

    /* @ElementCollection(targetClass=Package.class)
        @CollectionTable(
                name="package",
                joinColumns=@JoinColumn(name="id_advertisement")
        )*/
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
   // @JoinTable(name="package", joinColumns={@JoinColumn(name="id_advertisement", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="id", referencedColumnName="id_advertisement")})
    @JoinColumn(name="id_advertisement")
    Set<Packages> pack = new HashSet<Packages>();
    //private Package pack;

    public long getAcquired() {
        return acquired;
    }

    public void setAcquired(long acquired) {
        this.acquired = acquired;
    }

    @Column
    private long acquired;

    public double getPrice_send() {
        return price_send;
    }

    public void setPrice_send(double price_send) {
        this.price_send = price_send;
    }

    @Column
    private double price_send;

    public Set<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(Set<Auction> auctions) {
        this.auctions = auctions;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="id_adv")
    Set<Auction> auctions = new HashSet<Auction>();

}
