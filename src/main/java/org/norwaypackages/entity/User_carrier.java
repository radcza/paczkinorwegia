package org.norwaypackages.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by radc on 2015-08-28.
 */
@Entity
@Table(name = "user_carrier")
public class User_carrier implements Serializable {
    @Id
    @Column(name="id", unique=true, nullable=false)
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign", parameters=@org.hibernate.annotations.Parameter(name="property", value="user"))
    private long id;
    @Column
    private String name;
    @Column
    private int city_id;
    @Column
    private String street_name;

    @Column
    private String phone_mobile;
    @Column
    private String phone_stationary;
    @Column
    private String www;
    @Column
    private int NIP;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }


    public String getPhone_mobile() {
        return phone_mobile;
    }

    public void setPhone_mobile(String phone_mobile) {
        this.phone_mobile = phone_mobile;
    }

    public String getPhone_stationary() {
        return phone_stationary;
    }

    public void setPhone_stationary(String phone_stationary) {
        this.phone_stationary = phone_stationary;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public int getNIP() {
        return NIP;
    }

    public void setNIP(int NIP) {
        this.NIP = NIP;
    }

    public short getInsurance() {
        return insurance;
    }

    public void setInsurance(short insurance) {
        this.insurance = insurance;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCar_img() {
        return car_img;
    }

    public void setCar_img(String car_img) {
        this.car_img = car_img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public short getRefrigerated() {
        return refrigerated;
    }

    public void setRefrigerated(short refrigerated) {
        this.refrigerated = refrigerated;
    }

    public String getNIP_doc() {
        return NIP_doc;
    }

    public void setNIP_doc(String NIP_doc) {
        this.NIP_doc = NIP_doc;
    }

    public String getRegon_doc() {
        return Regon_doc;
    }

    public void setRegon_doc(String regon_doc) {
        Regon_doc = regon_doc;
    }

    public String getKRS_doc() {
        return KRS_doc;
    }

    public void setKRS_doc(String KRS_doc) {
        this.KRS_doc = KRS_doc;
    }

    @Column
    private short insurance;
    @Column
    private String logo;
    @Column
    private String car_img;
    @Column
    private String description;
    @Column
    private int capacity;
    @Column
    private short refrigerated;
    @Column
    private String NIP_doc;
    @Column
    private String Regon_doc;
    @Column
    private String KRS_doc;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;


}