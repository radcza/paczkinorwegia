package org.norwaypackages.controller;

import org.norwaypackages.entity.User;
import org.norwaypackages.service.SessionUtils;
import org.norwaypackages.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by radc on 2015-09-30.
 */
@Controller
public class LoginController {
    @Autowired
    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    @RequestMapping(value = { "/", "/index**" }, method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Logowanie");
        model.addObject("message", "Witaj w serwisie Paczki Norwegia");
        model.setViewName("index");
        return model;

    }

    /*@RequestMapping("/login")
    public ModelAndView login(@ModelAttribute User user, @RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout){

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Niepoprawna nazwa użytkownika lub hasło!");
        }

        if (logout != null) {
            model.addObject("msg", "Zostałeś wylogowany poprawnie.");
        }
        model.setViewName("login");

        return model;
    }*/
    @RequestMapping(value="/login")
    public String login(HttpServletRequest request, Model model){
        return "login";
    }

    @RequestMapping(value="/logout")
    public String logout(HttpServletRequest request, Model model){
        SessionUtils.logout(request);
        return "login";
    }

    @RequestMapping(value="/denied")
    public String denied(){
        return "denied";
    }
}
