package org.norwaypackages.controller;

//import com.mchange.io.FileUtils;
import org.hibernate.SessionFactory;
import org.norwaypackages.entity.City;
import org.norwaypackages.entity.User;
//import org.norwaypackages.repositories.UserRepository;
import org.norwaypackages.repository.JpaUserRepository;
import org.norwaypackages.service.CityService;
import org.norwaypackages.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.util.List;

/**
 * Created by Radek on 2015-07-14.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CityService cityService;
    @Autowired
    private JpaUserRepository userRepository;
    @Autowired
    private SessionFactory sessionFactory;
    @RequestMapping("createUser")
    public ModelAndView createUser(@ModelAttribute User user, City city) {
        List CityList = cityService.getAllCities();
        return new ModelAndView("userForm", "CityList", CityList/*,"user",new User()*/);
    }
    @RequestMapping("editUser")
    public ModelAndView editUser(@RequestParam long id, @ModelAttribute User user) {
        user = userService.getUser(id);
        return new ModelAndView("userForm", "userObject", user);
    }
    @RequestMapping(value = "saveUser")
    public ModelAndView saveUser(/*@ModelAttribute("user")*/ @Valid @ModelAttribute("user") User user, BindingResult bindingResult) {
    //Roles roles= new Roles();
        // Save the user to database
        user.getUser_send().setUser(user);
        //roles.setId_user(21);
        //roles.setUsername("rad");
        //roles.setRole("ROLE_USER");
        //user.setRoles(roles);
        user.getRoles().setUser(user);
        user.getRoles().setUsername(user.getNick());

        //sessionFactory.getCurrentSession().save(roles);

             userService.createUser(user);
        //roles.setIdUser(user.getId());
        //roles.setUsername(user.getNick());

       // return new ModelAndView("profile","userObject",user);
        return new ModelAndView("redirect:login?success=true");
    }

    @RequestMapping(value = "saveUser2")
    public ModelAndView saveUser2(@Valid @ModelAttribute("user") User user, BindingResult bindingResult,
                                  @RequestParam(value="user_carrier.logo",required=false) MultipartFile file){

        // Save the user to database

        user.getUser_carrier().setUser(user);

    user.setTelephone(user.getUser_carrier().getPhone_mobile());



       if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                //String rootPath = System.getProperty("catalina.home");
                //File dir = new File(rootPath + File.separator + "tmpFiles");
                File dir = new File("/resources/tmpFiles");
                if (!dir.exists())
                    dir.mkdirs();

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                        + "/" + user.getId() + ".jpg");
                user.getUser_carrier().setLogo(dir.getAbsolutePath()
                        + "/" + user.getId() + ".jpg");
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();


                //return "You successfully uploaded file=" + name;
            } catch (Exception e) {
                return new ModelAndView("userForm", "userObject", user);
            }
        }
        user.getRoles().setUser(user);
        user.getRoles().setUsername(user.getNick());
        userService.createUser(user);
        //return new ModelAndView("profile","userObject",user);
        return new ModelAndView("redirect:login?success=true");
    }


    @RequestMapping("deleteUser")
    public ModelAndView deleteUser(@RequestParam long id) {
        userService.deleteUser(id);
        return new ModelAndView("redirect:getAllUsers");
    }
    @RequestMapping(value = {"getAllUsers", "/users"})
    public ModelAndView getAllUsers() {
        List UserList = userService.getAllUsers();
        return new ModelAndView("UserList", "UserList", UserList);
    }
    @RequestMapping("profile")
    public ModelAndView showProfile(@ModelAttribute User user, Principal principal, @RequestParam(required=false) String nick) {
       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        if(nick==null)
            user=userRepository.findByNick(currentPrincipalName);
        else user=userRepository.findByNick(nick);
        ModelAndView model = new ModelAndView("profile","userObject",user);
        if(user.getPrivilege()==1) {City city = cityService.getCity(user.getUser_carrier().getCity_id());
            model.addObject("city", city);
        }
       // user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List comments=userService.getUserComments(user.getId());
        model.addObject("comments", comments);
        List postedComments=userService.getPostedComments(user.getId());
        model.addObject("postedComments", postedComments);
        return model;
    }
}
