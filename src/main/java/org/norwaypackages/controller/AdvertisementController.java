package org.norwaypackages.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.norwaypackages.DAO.PackagesDAO;
import org.norwaypackages.entity.*;
import org.norwaypackages.repository.JpaUserRepository;
import org.norwaypackages.service.AdvertisementService;
import org.norwaypackages.service.CityService;
import org.norwaypackages.service.PackagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.Serializable;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by Radek on 2015-07-23.
 */
@Controller
@PreAuthorize("hasRole('ROLE_USER')")
public class AdvertisementController {
    @Autowired
    private AdvertisementService advService;

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CityService cityService;
    @Autowired
    private JpaUserRepository userRepository;

    //dodawanie przesyłek
    @RequestMapping("createAdvertisement")
    public ModelAndView createAdvertisement(@ModelAttribute Advertisement adv, City city) {
        List CityList = cityService.getAllCities();
        ModelAndView model = new ModelAndView("AdvertisementForm", "CityList", CityList);
        model.addObject("type",1);
        return model;
    }

    @RequestMapping("createAdvertisement2")
    public ModelAndView createAdvertisement2(@ModelAttribute Advertisement adv, City city) {
        List CityList = cityService.getAllCities();
        ModelAndView model = new ModelAndView("AdvertisementForm", "CityList", CityList);
        model.addObject("type",2);
        return model;
    }

    @RequestMapping("editAdvertisement")
    public ModelAndView editAdvertisement(@RequestParam long id, @ModelAttribute Advertisement adv) {
        adv = advService.getAdvertisement(id);
        return new ModelAndView("index", "advertisementObject", adv);
    }

    @RequestMapping("saveAdvertisement")
    public ModelAndView saveAdvertisement(@ModelAttribute Advertisement adv, Packages packages) {
        /*if(adv.getId() == 0){
            advService.createAdvertisement(adv);
        } else {
            advService.updateAdvertisement(adv);
        }*/
        //obliczenie price, user_id, time
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user=userRepository.findByNick(currentPrincipalName);
        adv.setUser_id(user.getId()); //user_id
        int days=adv.getEnd().toLocalDate().getDayOfYear()-adv.getStart().toLocalDate().getDayOfYear();
        double price=5*days;
        adv.setPrice(price);
        adv.setCity_from(cityService.getCity(adv.getCity_id_from()));
        adv.setCity_to(cityService.getCity(adv.getCity_id_to()));
        advService.createAdvertisement(adv);
        ModelAndView model = new ModelAndView("packageForm", "advertisement", adv);
        model.addObject("type",adv.getType());
        return model;
    }


    @RequestMapping("deleteAdvertisement")
    public ModelAndView deleteAdvertisement(@RequestParam long id) {
        advService.deleteAdvertisement(id);
        return new ModelAndView("redirect:getAllAdvertisements");
    }
    @RequestMapping(value = {"getAllAdvertisements", "/index"})
    public ModelAndView getAllAdvertisements(Principal principal, @RequestParam(value = "type", required = false) Integer type) {
      //  ModelAndView model = new ModelAndView("advertisementsList");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user=userRepository.findByNick(currentPrincipalName);
        ModelAndView model = new ModelAndView("index","userObject",user);
        if(type==null) type=3;
        List AdvertisementList;
        if(type==1) {
            AdvertisementList = advService.getSelectedAdvertisements(112,"");
        }
        else if(type==2){
            AdvertisementList = advService.getSelectedAdvertisements(113,"");
        }
        else {AdvertisementList = advService.getAllAdvertisements();}
        Map<Long, Long> nrBidders = advService.getNrBidding();
        Map<Long, String> winningUser = advService.getUserWinning();
        Map<Long, Double> avgopinions = advService.getAverageOpinion();
        model.addObject("AdvertisementList", AdvertisementList);
        model.addObject("nrBidders",nrBidders);
        model.addObject("winningUser",winningUser);
        model.addObject("avgOpinions",avgopinions);
        model.addObject("type",type);
        return model;
    }

    @RequestMapping("getSelectedAdvertisements")
    public ModelAndView getSelectedAdvertisements(@RequestParam(value = "contain", required = false) String contain, @RequestParam(value = "contain2", required = false) String contain2, @RequestParam(value = "filter", required = false) Integer filter, @RequestParam(value = "myoffers", required = false) Integer myoffers, Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user=userRepository.findByNick(currentPrincipalName);
        ModelAndView model = new ModelAndView("index","userObject",user);
        if(!contain2.equals("")) contain=contain2;
        List AdvertisementList = advService.getSelectedAdvertisements(filter, contain);
        model.addObject("AdvertisementList", AdvertisementList);
        model.addObject("myoffers", myoffers);
        return model;
    }

    @RequestMapping("bid")
    public ModelAndView bid(@RequestParam(value = "id_user", required = true) long id_user, @RequestParam(value = "id_adv", required = true) long id_adv, @RequestParam(value = "price", required = true) float price) {
//@RequestParam(value = "id_user", required = true) long id_user, @RequestParam(value = "new_price", required = true) float new_price, @RequestParam(value = "id_advertisement", required = true) long id_advertisement
        Advertisement adv=advService.getAdvertisement(id_adv);
        //find lowest price for auction
        if(price<adv.getPrice_send() && advService.findMinPrice(id_adv)>price ) {
Auction auction=new Auction();
            auction.setIdAdv(id_adv);
            auction.setIdUser(id_user);
            auction.setPrice(price);
            java.util.Date date= new java.util.Date();
            auction.setDateTime(new Timestamp(date.getTime()));
            advService.saveAuction(auction);
            adv.setAcquired(id_user);
            BestAuction bestauction=new BestAuction();
            bestauction.setId_adv(id_adv);
            bestauction.setId_user(id_user);
            bestauction.setPrice(price);
            advService.updateBestAuction(id_adv, bestauction);
            //advService.updateAdvertisement(adv);
        }
        //jeśli data ogłoszenia się skończyła weź użytkownika z najniższą kwotą z tabeli auction
        //to-do
        //adv.setAcquired(id_user);
        //advService.updateAdvertisement(adv);
        return new ModelAndView("redirect:getAllAdvertisements");
    }

    @RequestMapping("acquire")
    public ModelAndView acquire(@RequestParam(value = "id_user", required = true) long id_user, @RequestParam(value = "id_advertisement", required = true) long id_advertisement) {
        //model.addAttribute("advertisement", new Advertisement());
        //update acquired in advertisement
        Advertisement adv=advService.getAdvertisement(id_advertisement);
        adv.setAcquired(id_user);
        advService.updateAdvertisement(adv);
        return new ModelAndView("redirect:getAllAdvertisements");
    }


    @RequestMapping("addToObserved")
    public ModelAndView addToObserved(@RequestParam(value = "id_user", required = true) long id_user, @RequestParam(value = "id_advertisement", required = true) long id_advertisement) {

        Observed observed=new Observed();
        observed.setId_adv(id_advertisement);
        observed.setId_user(id_user);
        advService.saveObserved(observed);
        return new ModelAndView("redirect:getAllAdvertisements");
    }

    @RequestMapping("addComment")
    public ModelAndView addComment(@RequestParam(value = "id_user", required = true) long id_user, @RequestParam(value = "id_advertisement", required = true) long id_advertisement, @RequestParam(value = "addedby", required = true) long addedBy, @RequestParam(value = "description", required = true) String description, @RequestParam(value = "points", required = true) byte points, @RequestParam(value = "dateTime", required = true) Timestamp dateTime) {
System.out.println("TUTAJ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Opinion opinion=new Opinion();
        opinion.setId_adv(id_advertisement);
        opinion.setUserId(id_user);
        opinion.setDateTime(dateTime);
        opinion.setDescription(description);
        opinion.setAddedby(addedBy);
        opinion.setPoints(points);
        advService.saveOpinion(opinion);
        return new ModelAndView("redirect:getAllAdvertisements");
    }

   /* @RequestMapping(value = "/pages/{pageNumber}", method = RequestMethod.GET)
    public String getAdvertisementPage(@PathVariable Integer pageNumber, Model model) {
        Page<Advertisement> page = advService.getAdvertisements(pageNumber);

        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());

        model.addAttribute("index", page);
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);

        return "index";
    }*/
}
