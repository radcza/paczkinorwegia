package org.norwaypackages.controller;

import org.norwaypackages.entity.City;
import org.norwaypackages.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by radc on 2015-07-18.
 */
@Controller
public class CityController {
    @Autowired
    private CityService cityService;

    @RequestMapping("createCity")
    public ModelAndView createCity(@ModelAttribute City city) {
        return new ModelAndView("CityForm");
    }
    @RequestMapping("editCity")
    public ModelAndView editCity(@RequestParam int id, @ModelAttribute City city) {
        city = cityService.getCity(id);
        return new ModelAndView("citiesList", "cityObject", city);
    }
    @RequestMapping("saveCity")
    public ModelAndView saveCity(@ModelAttribute City city) {
        if(city.getId() == 0){
            cityService.createCity(city);
        } else {
            cityService.updateCity(city);
        }
        return new ModelAndView("redirect:getAllCities");
    }
    @RequestMapping("deleteCity")
    public ModelAndView deleteCity(@RequestParam int id) {
        cityService.deleteCity(id);
        return new ModelAndView("redirect:getAllCities");
    }
    @RequestMapping(value = {"getAllCities", "/cities"})
    public ModelAndView getAllCities() {
        List CityList = cityService.getAllCities();
        return new ModelAndView("citiesList", "CityList", CityList);
    }
}
