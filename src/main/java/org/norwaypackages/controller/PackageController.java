package org.norwaypackages.controller;

import org.norwaypackages.entity.Packages;
import org.norwaypackages.service.PackagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by radc on 2015-11-28.
 */
@Controller
public class PackageController {
    @Autowired
    private PackagesService packService;

    @RequestMapping("savePackages")
    public ModelAndView savePackages(@ModelAttribute Packages packages) {


        packService.createPackage(packages);
        return new ModelAndView("index");
    }
}
