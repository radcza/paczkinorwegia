package org.norwaypackages.DAO;

import org.norwaypackages.entity.Packages;

/**
 * Created by radc on 2015-11-28.
 */
public interface PackagesDAO {
    public long createPackage(Packages pack);
}
