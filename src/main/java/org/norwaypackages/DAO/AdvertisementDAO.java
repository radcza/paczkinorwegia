package org.norwaypackages.DAO;

import org.norwaypackages.entity.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Radek on 2015-07-22.
 */
public interface AdvertisementDAO {
    public long createAdvertisement(Advertisement adv);
    public BestAuction saveBestAuction(BestAuction ba);
    public Advertisement updateAdvertisement(Advertisement adv);
    public BestAuction updateBestAuction(long id, BestAuction ba);
    public void deleteAdvertisement(long id);
    public List getAllAdvertisements();
    public List getSelectedAdvertisements(int sortby, String search);
    public Advertisement getAdvertisement(long id);
    public BestAuction getBestAuction(long id);
    public User getUser(long user_id);
    public double findMinPrice(long id_advertisement);
    public User findWinningUser(long id_advertisement);
    public Auction saveAuction(Auction auction);
    public Map<Long, Long> getNrBidding();
    public Map<Long, String> getUserWinning();
    public Observed saveObserved(Observed observed);
    public Opinion saveOpinion(Opinion opinion);
    public Map<Long, Double> getAverageOpinion();
}
