package org.norwaypackages.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.norwaypackages.entity.Opinion;
import org.norwaypackages.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Collection;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * Created by Radek on 2015-07-13.
 */
@SuppressWarnings("ALL")
@Repository
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public long createUser(User user) {
        Serializable id = sessionFactory.getCurrentSession().save(user);
        return (long) id;
    }

    @Override
    public User updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
        return user;
    }

    @Override
    public void deleteUser(long id) {
        User user = new User();
        user.setId(id);
        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User");
        return query.list();
    }

    @Override
    public User getUser(long id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public List<Opinion> getUserComments(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Opinion where userId=:userId");
        query.setParameter("userId", id);
        return query.list();
    }

    @Override
    public List<Opinion> getPostedComments(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Opinion where addedby=:userId");
        query.setParameter("userId", id);
        return query.list();
    }

}
