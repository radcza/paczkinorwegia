package org.norwaypackages.DAO;

import org.hibernate.*;
import org.norwaypackages.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Radek on 2015-07-22.
 */
@SuppressWarnings("ALL")
@Repository
public class AdvertisementDAOImpl implements AdvertisementDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public long createAdvertisement(Advertisement adv) {

        Session session = sessionFactory.getCurrentSession();

        Serializable id = session.save(adv);
        return (long)id;
    }



    @Override
    public Advertisement updateAdvertisement(Advertisement adv) {
        sessionFactory.getCurrentSession().update(adv);
        return adv;
    }

    @Override
    public void deleteAdvertisement(long id) {
        Advertisement adv = new Advertisement();
        adv.setId(id);
        sessionFactory.getCurrentSession().delete(adv);
    }

    @Override
    public List<Advertisement> getAllAdvertisements() {
        //Session session = sessionFactory.getCurrentSession();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Advertisement");
        return query.list();
    }

    public List<Advertisement> getSelectedAdvertisements(int sortby, String search) {
        String sort;
        Query query=null;
        List<Advertisement> adv;
        Session session = sessionFactory.getCurrentSession();
        if(sortby==0){
        query = session.createQuery("FROM Advertisement a where a.city_from.name like :city_from or a.city_to.name like :city_to or a.user.nick like :user or a.acquired=(select id from User where nick like :user)");
        query.setParameter("city_from", search+"%");
            query.setParameter("city_to", search+"%");
            query.setParameter("user", search+"%");
            //query.setParameter("pack", search+"%");
        }
        else if(sortby==111){//wybierz tylko obserwowane

            query = session.createQuery("FROM Advertisement a where a.id IN (select id_adv from Observed where id_user = :id_user)");
            query.setParameter("id_user", Long.parseLong(search));
        }
        else if(sortby==112){//tylko paczki
            query = session.createQuery("FROM Advertisement a where a.type='1'");

        }
        else if(sortby==113){//tylko przewoźnicy
            query = session.createQuery("FROM Advertisement a where a.type='2'");

        }
        else{
            if(sortby==1) sort="start";
            else if(sortby==2) sort="end";
            else if(sortby==3) sort="city_from.name"; //// TODO: 2015-12-20
            else if(sortby==4) sort="city_to.name";
            else sort="start"; //// TODO: 2015-12-20
            query = session.createQuery("FROM Advertisement a where a.city_from.name like :city_from or a.city_to.name like :city_to or a.user.nick like :user or a.acquired=(select id from User where nick like :user) order by a."+sort+" asc");
            query.setParameter("city_from", search+"%");
            query.setParameter("city_to", search+"%");
            query.setParameter("user", search+"%");

           // query.setParameter("order", sort);
        }

        return query.list();
    }
    /*public List<Packages> getAllAdvertisements(Advertisement ad) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Packages where id_advertisement="+ad.getId());
        return query.list();
    }*/

    @Override
    public Advertisement getAdvertisement(long id) {
        return (Advertisement)sessionFactory.getCurrentSession().get(Advertisement.class, id);
    }

    @Override
    public User getUser(long user_id) {
        return (User)sessionFactory.getCurrentSession().get(User.class, user_id);
    }

    @Override
    public double findMinPrice(long id_advertisement) {

        SQLQuery query=sessionFactory.getCurrentSession().createSQLQuery("select min(price) FROM Auction where id_adv=:id_advertisement");
        query.setParameter("id_advertisement", id_advertisement);
        if(query.list().get(0)==null) {
            query = sessionFactory.getCurrentSession().createSQLQuery("select price_send FROM advertisement where id=:id_advertisement");
            query.setParameter("id_advertisement", id_advertisement);
            System.out.print("------------------"+query.list().get(0)+"--------------");
            return (double)query.list().get(0);
        }

        return (double)query.list().get(0);
    }

    @Override
    public User findWinningUser(long id_advertisement) {
        SQLQuery query=sessionFactory.getCurrentSession().createSQLQuery("select u FROM Auction a, user u where a.id_adv=:id_advertisement and min(a.price) and u.id=a.id_user");
        query.setParameter("id_advertisement", id_advertisement);
        return (User)query.list().get(0);
    }

    @Override
    public Auction saveAuction(Auction auction) {
        sessionFactory.getCurrentSession().save(auction);
        return auction;
    }

    public Map<Long, Long> getNrBidding() {
        Map<Long, Long> results = new HashMap<>();
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT ad.id, COUNT(DISTINCT au.idUser) FROM Advertisement ad, Auction au Where au.idAdv=ad.id group by au.idAdv");
        List<Object[]> resultList = query.list();
        // Place results in map
        for (Object[] borderTypes: resultList) {
            results.put((Long)borderTypes[0], (Long)borderTypes[1]);
        }

        return results;
    }
    public Map<Long, String> getUserWinning(){
        Map<Long, String> results = new HashMap<>();
        //Query query=sessionFactory.getCurrentSession().createQuery("select ad.id, u.nick FROM Advertisement ad, Auction a, User u where a.idAdv=ad.id and a.price=(SELECT min(a.price) FROM Auction a, Advertisement ad where a.idAdv=ad.id) and u.id=a.idUser");
        Query query=sessionFactory.getCurrentSession().createQuery("SELECT a.idAdv, u.nick, min(a.price) FROM Auction a, Advertisement ad, User u where a.idAdv=ad.id and u.id=a.idUser group by a.idAdv");

        List<Object[]> resultList = query.list();
        // Place results in map
        for (Object[] borderTypes: resultList) {
            results.put((Long)borderTypes[0], (String)borderTypes[1]);
        }

        return results;
    }

    @Override
    public BestAuction saveBestAuction(BestAuction ba) {
        sessionFactory.getCurrentSession().save(ba);
        return ba;
    }

    @Override
    public BestAuction getBestAuction(long id_adv) {
        BestAuction ba=null;
        ba=(BestAuction)sessionFactory.getCurrentSession().get(BestAuction.class, id_adv);

        return ba;
    }

    @Override
    public BestAuction updateBestAuction(long id, BestAuction bestAuction) {
        Session session = sessionFactory.getCurrentSession();
        BestAuction ba=getBestAuction(id);

        if (ba==null) { session.save(bestAuction); }
        else {
            ba.setPrice(bestAuction.getPrice());
            ba.setId_user(bestAuction.getId_user());
            session.update(ba);}
        /*session.flush();
        session.beginTransaction().commit();
        session.clear();*/
        return bestAuction;
    }

    public Observed getObserved(long id_adv){
        Query query = sessionFactory.getCurrentSession().createQuery("from Observed where id_adv=:id_adv");
        query.setParameter("id_adv", id_adv);
        Observed observed=null;
        if(!query.list().isEmpty()) observed=(Observed)query.list().get(0);
        return observed;
    }

    @Override
    public Observed saveObserved(Observed observed) {
        if (getObserved(observed.getId_adv())==null)
        sessionFactory.getCurrentSession().save(observed);
        return observed;
    }

    public Opinion getOpinion(long id_adv){
        Query query = sessionFactory.getCurrentSession().createQuery("from Opinion where id_adv=:id_adv");
        query.setParameter("id_adv", id_adv);
        Opinion opinion=null;
        if(!query.list().isEmpty()) opinion=(Opinion) query.list().get(0);
        return opinion;
    }

    @Override
    public Opinion saveOpinion(Opinion opinion) {
        if (getOpinion(opinion.getId_adv())==null)
            sessionFactory.getCurrentSession().save(opinion);
        return opinion;
    }

    @Override
    public Map<Long, Double> getAverageOpinion() {
        Map<Long, Double> results = new HashMap<>();
        Query query = sessionFactory.getCurrentSession().createQuery("select userId, AVG(points) from Opinion GROUP BY userId");
        //query.setParameter("user_id", user_id);
        List<Object[]> resultList = query.list();
        // Place results in map
        for (Object[] borderTypes: resultList) {
            results.put((Long)borderTypes[0], (Double)borderTypes[1]);
        }

        return results;
      //  return (double)query.list().get(0);
    }
}
