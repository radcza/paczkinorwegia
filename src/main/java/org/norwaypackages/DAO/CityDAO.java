package org.norwaypackages.DAO;

import org.norwaypackages.entity.City;
import org.norwaypackages.entity.User;

import java.util.List;

/**
 * Created by radc on 2015-07-18.
 */
public interface CityDAO {
    public int createCity(City city);
    public City updateCity(City city);
    public void deleteCity(int id);
    public List getAllCities();
    public City getCity(int id);
}
