package org.norwaypackages.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.norwaypackages.entity.Packages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created by radc on 2015-11-28.
 */
@SuppressWarnings("ALL")
@Repository
public class PackagesDAOImpl implements PackagesDAO{
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public long createPackage(Packages pack) {

        Session session = sessionFactory.getCurrentSession();
        //Query query = session.createQuery("FROM City c Where c.id=adv.id_from OR c.id=adv.id_to");
        //Hibernate.initialize(adv.getCities().addAll(query.list()));
        Serializable id = session.save(pack);
        return (long)id;
    }
}
