package org.norwaypackages.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.norwaypackages.entity.City;
import org.norwaypackages.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by radc on 2015-07-18.
 */
@SuppressWarnings("ALL")
@Repository
public class CityDAOImpl implements CityDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public int createCity(City city) {
        Serializable id = sessionFactory.getCurrentSession().save(city);
        return (int)id;
    }

    @Override
    public City updateCity(City city) {
        sessionFactory.getCurrentSession().update(city);
        return city;
    }

    @Override
    public void deleteCity(int id) {
        City city = new City();
        city.setId(id);
        sessionFactory.getCurrentSession().delete(city);
    }

    @Override
    public List<City> getAllCities() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM City");
        return query.list();
    }

    @Override
    public City getCity(int id) {
        return (City)sessionFactory.getCurrentSession().get(City.class, id);
    }
}
